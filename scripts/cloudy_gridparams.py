from pathlib import Path
import pprint






MODEL_ROOT= "/Users/michaeltopping/software/cloudy/c17.01/zneb100_zstar014/"
DELIMIT = "**************************************************"
pp = pprint.PrettyPrinter(indent=4)



def get_grid(filename):

    outfile = Path(MODEL_ROOT+filename)

    inparams = False
    startsearch = False
    params = {'0':[]}
    gridnum = 0
    lastline = "abc"

    # open and read through the file
    with outfile.open() as f:
        for line in f:
            if 'GRID_DELIMIT' in line.strip():
                break
            if 'Producing grid output' in line.strip():
                startsearch = True
            if not startsearch:
                continue

            if DELIMIT in line.strip():
                print(line.strip(), lastline)
                if lastline == line.strip():
                    break
                if inparams:
                    gridnum += 1
                    params['{}'.format(gridnum)] = []
                    lastline = line.strip()
                    continue
                else:
                    inparams = True

            if inparams:
                params['{}'.format(gridnum)].append(line.strip())
            lastline = line.strip()


    # a random kludge
    params['0'] = params['0'][1:]
    pp.pprint(params)







if __name__ == "__main__":
    get_grid("zneb100_zstar014_example.out")
