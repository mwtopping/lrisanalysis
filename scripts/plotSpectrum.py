import matplotlib.pyplot as plt
from analyzefits import read_spectrum, get_spectra, splice_spectrum
from matplotlib import rc
rc('font', **{'serif': ['Computer Modern'], 'size': 16})
rc('text', usetex=True)

fig = plt.figure(figsize=(12,5))
mask = 'gs_l1'
obj = '36705'
loc = '/Users/michaeltopping/lrisData/gs_l1-combine/'

fnameb = loc+"b/split/{}.b.{}.msdfc_vs.fits".format(mask, obj)
fnamer = loc+"r/split/{}.r.{}.msdfc_vs.fits".format(mask, obj)

z = 2.3064


ws_b, spectra_b, ws_r, spectra_r = get_spectra(fnameb, fnamer)
ws, spectrum = splice_spectrum(ws_b, spectra_b, ws_r, spectra_r)

lines = [r'Ly$\alpha$', r'SiII','OI+SiII', 'CII', 'SiII', 'CIV']
linews = [1215.67, 1260.42,1303, 1334.53, 1526.71, 1550, 1393.76, 1402.77]
for w in linews:
    plt.plot([w, w], [2.0e-29, 3.0e-29], 'b')
#    plt.text(w, 3.0e-29, l, horizontalalignment='center')


plt.plot(ws/(1+z), spectrum, 'k', linewidth=1)
plt.xlim([900,1800])
plt.ylim([-0.1e-29, 0.35e-28])
plt.xlabel('Wavelength \AA')
plt.show()

