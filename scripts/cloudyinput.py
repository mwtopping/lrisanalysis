from math import log10
import numpy as np
import sys


def logno(X): return -1.1 + 0.7*(X - 8.0) # X = 12 + log(O/H)

def add_title(outputfile, metallicity, age, zneb, n):
    outputfile.write('title neb emission with bpass zneb={} zstar={} age={} U={}\n'.format(metallicity, zneb, age, n))


def add_spectrum(outputfile, age, metallicity):
    outputfile.write('table star "bpass_v2p2.1/bpass_v2p2.1_imf_chab100_cont_binary.mod" 1e{} {:.4f}\n'.format(age, log10(metallicity)))


def add_density(outputfile):
    outputfile.write('hden linear 250\n')


def add_ionization(outputfile, n):

    outputfile.write('ionization paramater -2.5\n')
#    if grid:
#        outputfile.write('ionization paramater -2.5 vary\n')
#        outputfile.write('grid from -4 to -1 in 0.2 dex steps\n')
#    else:
#        outputfile.write('ionization paramater -2.5\n')

def add_nebZ(outputfile, zneb):
    outputfile.write('metals {}\n'.format(10**zneb))
#    outputfile.write('metals 0. vary\n')
#    outputfile.write('grid from -2. to 0.4 in 0.2 dex steps\n')

def add_output(outputfile, age, metallicity, nebz, n):
    outputfile.write('iterate to convergence\n')
    outputfile.write('save line list "bpass{}_zstar{}_zneb{:.2f}_U{:.2f}_age{:.1f}_linelist_intrinsic.txt"' \
                     ' "linelist.dat" last\n'.format('chab100', metallicity, zneb, n, age))
    outputfile.write('save continuum "bpass{}_zstar{}_zneb{:.2f}_U{:.2f}_age{:.1f}_continuum.dat"' \
                     ' units Angstrom\n'.format('chab100', metallicity, zneb, n, age))

def add_nsf(outputfile, zneb):
    lognogrid = logno(8.69 + zneb)
    logohgrid = -3.31 + zneb
    lognhgrid = -4.07 + zneb
    nsf = 10.**(lognogrid + logohgrid - lognhgrid)
    outputfile.write('element scale factor nitrogen {}\n'.format(nsf))



def create_input(age, metallicity, zneb, n):
    loc = '../data/cloudyfiles/'
    filename = 'bpass{}_zstar{}_zneb{:.2f}_U{:.2f}_age{:.1f}.in'.format('chab100', metallicity, zneb,n, age)
    outputfile = open(loc+filename, 'w')
    add_title(outputfile, metallicity, age, zneb, n)
    add_spectrum(outputfile, age, metallicity)
    add_density(outputfile)
    add_ionization(outputfile, n)
    add_nebZ(outputfile, zneb)
    add_output(outputfile, age, metallicity, zneb, n)
    add_nsf(outputfile, zneb)
    outputfile.close()
    # print out the code to run the scripts
#    print("os.system('/Users/michaeltopping/software/cloudy/c17.01/source/cloudy.exe -r {}')".format(filename))
    return filename


if __name__ == "__main__":
    metals = [1e-5, 1e-4, 1e-3, 0.002, 0.003, 0.004, 0.006, 0.008, 0.010, 0.014, 0.020, 0.030, 0.040]
    dz = 0.2
    nebmetals = np.arange(-2, .2+dz, dz)
    ionization = [-2.5, -2.0, -3.0, -1.0, -4.0, -2.4, -2.6, -2.2, -2.8, -3.2, -1.8, -3.4, -1.6, -3.6, -1.4, -3.8, -1.2]
#    nebmetals = 10**nebmetals
    dage = 0.4
    ages = np.arange(7, 9.6+dage, dage)
    # loop through the age and stellar metallicity
    

    for n in ionization:
        #open a new script file
        runscript = open('run_grid_U{:.1f}.py'.format(n), 'w')
        runscript.write('import os\n')
        runscript.write('import time\n')
        runscript.write('from tqdm import tqdm\n')
        runscript.write('modelno = 0\n')
        runscript.write('starttime = time.time()\n')
        runscript.write('filenames = [')
        for age in ages:
            for z in metals:
                for zneb in nebmetals:
                    runscript.write("'{}',".format(create_input(age, z, zneb, n)[:-3]))
                    

        runscript.write(']\n')

        runscript.write('totmodels = len(filenames)\n')
        runscript.write('for fname in filenames:\n')
    #    runscript.write("    os.system('/Users/michaeltopping/software/cloudy/c17.01/source/cloudy.exe -r {}'.format(fname))")
        runscript.write("    os.system('/Users/michaeltopping/software/cloudy/c17.01/source/cloudy.exe -r {}'.format(fname))\n")
        runscript.write("    timeleft = (time.time()-starttime)*totmodels/(modelno+1)/3600.0\n")
        runscript.write("    modelno += 1\n")
        runscript.write("    print('finished {}/{}, {:.2f}h remaining'.format(modelno, totmodels, timeleft))\n")
