from fit_z import read_spectrum, get_spectra, splice_spectrum, load_data
import numpy as np


MIN = 1560
MAX = 1600


def get_snr(obj, mask, z, loc):
    # assemble the filenames
    fnameb = loc+"b/split/{}.b.{}.msdfc_vs.fits".format(mask, obj)
    fnamer = loc+"r/split/{}.r.{}.msdfc_vs.fits".format(mask, obj)
    fnamebsig = loc+"b/split/{}.bsig.{}.msdfc_vs.fits".format(mask, obj)
    fnamersig = loc+"r/split/{}.rsig.{}.msdfc_vs.fits".format(mask, obj)



    # read in all the spectra and error spectra
    ws_b, spectra_b, ws_r, spectra_r = get_spectra(fnameb, fnamer)
    ws_bsig, spectra_bsig, ws_rsig, spectra_rsig = get_spectra(fnamebsig, fnamersig)

    # put the spectra together
    ws, spectrum = splice_spectrum(ws_b, spectra_b, ws_r, spectra_r)
    ws_sig, spectrum_sig = splice_spectrum(ws_bsig, spectra_bsig, ws_rsig, spectra_rsig)

    lindex = np.abs(ws - (MIN*(1+z))).argmin()
    rindex = np.abs(ws - (MAX*(1+z))).argmin()

    snr = np.average(spectrum[lindex:rindex] / spectrum_sig[lindex:rindex])

    return snr




def main():

    locs, objs = load_data()

    masks = ['gs_l1', 'co_l1', 'co_l2', 'gn_l1', 'ae_l1', 'co_l5', 'co_l6', 'ae_l3', 'gn_l3']


    snrcat = open('snr.txt', 'w')

    cat_mosdef = np.genfromtxt('/Users/michaeltopping/Documents/mosdef_zcat.final.dat', usecols=(2, 5))
    mosdef = {} 
    for row  in cat_mosdef:
        mosdef["{}".format(int(row[0]))] = row[1]
 
    addtl_zs = np.genfromtxt('morezs.init')
    for row in addtl_zs:
        mosdef["{}".format(int(row[0]))] = row[1]
 
 
 
    for mask in masks:
        loc = locs[mask]
        for obj in objs[mask]:


            if obj in mosdef:
                z = mosdef[obj]
                try:
                    snr = get_snr(obj, mask, z, loc)
                    snrcat.write("{} {}\n".format(obj, snr))
                except FileNotFoundError:
                    snrcat.write("{} 0\n".format(obj))
                    


    snrcat.close()


if __name__ == "__main__":
    main()
