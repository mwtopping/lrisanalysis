import pathlib
import os
import astropy.io.fits
from collections import OrderedDict
import numpy as np
import matplotlib.pyplot as plt


# this is a procedure that readin in the fitting parameters from a file
# INPUT: filename - the filename of the fitting parameter file
def read_fits(filename):

    fits = OrderedDict({}) 

    fitsfile = pathlib.Path(filename)

    mask = "aaaaa"
    specline = "bbbbb"
    obj = "99999"
    
    # open the file and loop through the lines
    with fitsfile.open() as f:
        for line in f:
            # this will be a line that contains an object identifier
            if line.startswith('#'):
                line = line[1:].strip().split('.')
                mask = line[0]
                obj = line[1]
                # if this is the first object that has been seen in this mask, create the data structure to hold it
                if not mask in fits:
                    fits[mask] = {}
                
                # create the data structure to hold the object fitting data
                fits[mask][obj] = {}

            # check if the first character is a letter, this will be the start of a new line measurement
            elif line[0].isalpha() and not (line.split()[0] == 'nan'):

                specline = ''.join(i for i in line if (i.isalpha() or i == '+'))
                fits[mask][obj][specline] = []

            # this will be a line that is fitting parameters
            elif (line[0].isdigit() or line[0] == '-'):
                fits[mask][obj][specline].append(line.strip())


    return fits
            

# read in the fits data and return the spectrum and wavelength information
# INPUT:  filename - fits file of spectrum
# OUTPUT: data  - data array containing the spectrum data
#         CRVAL - wavelength of the first pixel
#         CDELT - wavelength spacing of the data
#         naps  - number of apertures in the file
def read_spectrum(filename):
    # read the data into a buffer
    buf = astropy.io.fits.getdata(filename)
    ap=1
    
    # check if the data has more than one aperture
    if len(np.array(buf).shape) > 1:
        data = buf
        naps = np.array(buf).shape[0]
    else:
        data = buf
        naps = 1
    # get the header data
    header = astropy.io.fits.getheader(filename)

    # read in the wavelength solution if there is only one aperture
    if 'CRVAL1' in header.keys():
        CRVAL = header['CRVAL1'] #zero point
        CDELT = header['CDELT1']

    # there are more than 1 apertures
    else:
        
        sol = header["WAT2_00{}".format(ap)].strip()
    
        sol = sol.split('{} {} 0'.format(ap, ap))
        sol = sol[-1].strip().split(" ")

        CRVAL=float(sol[0])
        CDELT=float(sol[1])

    return data, CRVAL, CDELT, naps

# read in the spectra from the red and blue side of lris
# INPUT: bfilename - filename of the blue side spectrum
#        rfilename - filename of the red side spectrum
# OUTPUT: ws_b      - wavelength array for the blue side spectrum
#         spectra_b - data for the blue side spectrum
#         ws_r      - wavelength array for the red side spectrum
#         spectra_r - data array for the red side spectrum
def get_spectra(bfilename, rfilename):
    # make sure the blue side spectrum exists
    try:
        data, CRVAL, CDELT, naps = read_spectrum(bfilename)
        ws_b = CRVAL + CDELT*np.arange(len(data))
        spectra_b = data
    except FileNotFoundError:
        print("Missing {}".format(bfilename))
        ws_b = []
        spectra_b = []

    # make sure the red side spectrum exists
    try:
        data, CRVAL, CDELT, naps = read_spectrum(rfilename)
        ws_r = CRVAL + CDELT*np.arange(len(data))
        spectra_r = data
    except FileNotFoundError:        
        print("Missing {}".format(rfilename))
        ws_r = []
        spectra_r = []


    return ws_b, spectra_b, ws_r, spectra_r



# given a red and blue side spectrum this will put them together into a single spectrum
def splice_spectrum(ws_b, spectra_b, ws_r, spectra_r):

    LAMBDA_MID = 5100

    # check to make sure at least one side of the spectra is available
    if len(spectra_b) == 0 and len(spectra_r) == 0:
        print("No spectra available")
        raise FileNotFoundError

    # this is if there is only a red side spectrum
    elif len(spectra_b) == 0:

        rindex = np.array([x for x in ws_r - LAMBDA_MID if x < 0]).argmax()+1 # index to the right of the middle
        ws = ws_r[rindex:] 
        spectrum = spectra_r[rindex:]

    # this is if there is only a blue side spectrum
    elif len(spectra_r) == 0:

        bindex = np.array([x for x in ws_b - LAMBDA_MID if x < 0]).argmax() # index to the left of the middle
        ws = ws_b[:bindex] 
        spectrum = spectra_b[:bindex]


    # there are both red and blue spectra
    else:
        # make sure the dichroic cutoff is within the spectra, otherwise, dont cut the spectrum
        try:
            rindex = np.array([x for x in ws_r - LAMBDA_MID if x < 0]).argmax()+1 # index to the right of the middle
        except ValueError:
            rindex = 0
        try:
            bindex = np.array([x for x in ws_b - LAMBDA_MID if x < 0]).argmax() # index to the left of the middle
        except ValueError:
            bindex = 0
        ws = np.append(ws_b[:bindex], ws_r[rindex:])
        spectrum = np.append(spectra_b[:bindex], spectra_r[rindex:])

    return ws, spectrum


# load in all of the object identifiers and data directory locations
def load_data():
    objs = {}
    locs = {}
    objs['gs_l1'] = ['31791','31344','32837','36705','40768','37988','31854','33248','41547','45188','35178','40679','42556','39198','41218','41886','35705','46335','34114','38119','42363','38559','45180','46938','35779','42809','45531','39713','40218','38116']
    locs['gs_l1'] = "/Users/michaeltopping/lrisData/gs_l1-combine/"

    objs['co_l1'] = ['11968','11530','11443','11153','10056','10143','9801','9094','8540','8338','8081','7912','7273','6963','6826','6417','5912','5686','5462','4945','4154','3694','3112','2672','2786','2207','1908','1740','1382','964','541','307','241']
    locs['co_l1'] = "/Users/michaeltopping/lrisData/lris_jan17/co_l1/"

    objs['co_l2'] = ['16545','16547','17038','17233','18067','19985','20062','19439','19712','21743','20171','21780','22939','21955','23134','23663','23183','23210','23841','24427','24020','24414','24053','24738','26073','25322','27216','27120' ,'26332','28258','27906']
    locs['co_l2'] = "/Users/michaeltopping/lrisData/lris_jan17/co_l2/"

    objs['gn_l1']=['30053','32526','29743','26621','30461','31955','30709','28846','29834','21772','23344','27035','23869','18128','25142','21279','17958','21845','22023','20924','16713','19654','18161','15186','12980','12157','10596','12345','10645']
    locs['gn_l1'] = "/Users/michaeltopping/lrisData/lris_apr17/gn_l1/"

    objs['ae_l1']=['30074','36257','36451','33808','32354','24481','38356','23409','34661','23040','28710','21675','17437','16496','16730','15082','25522','16121','14957','26153','12918','17085','10471','10494','3668','6569','11729','14880','4711','6311','3112']
    locs['ae_l1']= "/Users/michaeltopping/lrisData/lris_apr17/ae_l1/"

    objs['co_l5']=['4078','4497','3666','6018','4446','3974','6379','3626','7430','7735','3185','6283','851','6179','5107','7883','3324','4029','10066','10085','4    441','3757','8697','4156','5162','6743']
    locs['co_l5'] = "/Users/michaeltopping/lrisData/lris_jan2018/co_l5/"

    objs['co_l6']=['13101','13299','15710','12476','11716','13364','12577','12611','14423','10235','11343','10280','9044','10835','9148','10093','8679','8515','64    13','9251','5814','8232','6817','5901','5571','4962']
    locs['co_l6'] = "/Users/michaeltopping/lrisData/lris_jan2018/co_l6/"

    objs['gn_l3'] = ['21617','11892','14739','17940','17204','22235','17667','19067','19350','16351','17714','17530','22299','25498','24825','24388','22669','24846','25505','22487','23674','25688','28237','28599','26557','29095','24328']
    locs['gn_l3'] = "/Users/michaeltopping/lrisData/gn_l3-combine/"

    objs['ae_l3']=['33768','28659','34308','31226','33801','22931','20924','39897','30278','40851','28421','27825','18543','33973','34813','32638','34848','37226','32761','24341','27627','35262','25817','29650','33942']
    locs['ae_l3'] = "/Users/michaeltopping/lrisData/ae_l3-combine/"

    return locs, objs


def get_spectrum(obj, mask):
    locs, objs = load_data()
    
    loc = locs[mask]
 
    fnameb = loc+"b/split/{}.b.{}.msdfc_vs.fits".format(mask, obj)
    fnamer = loc+"r/split/{}.r.{}.msdfc_vs.fits".format(mask, obj)
    fnamebsig = loc+"b/split/{}.bsig.{}.msdfc_vs.fits".format(mask, obj)
    fnamersig = loc+"r/split/{}.rsig.{}.msdfc_vs.fits".format(mask, obj)
 
    # load in and accumulate all of the spectra
    ws_b, spectra_b, ws_r, spectra_r = get_spectra(fnameb, fnamer)
    ws_bsig, spectra_bsig, ws_rsig, spectra_rsig = get_spectra(fnamebsig, fnamersig)

    ws, spectrum = splice_spectrum(ws_b, spectra_b, ws_r, spectra_r)
    ws_sig, spectrum_sig = splice_spectrum(ws_bsig, spectra_bsig, ws_rsig, spectra_rsig)

    if len(spectrum) < len(spectrum_sig):
        spectrum_sig = spectrum_sig[0:len(spectrum)]
    elif len(spectrum) > len(spectrum_sig):
        spectrum = spectrum[0:len(spectrum_sig)]
        ws = ws[0:len(spectrum_sig)]


    return ws, spectrum, spectrum_sig
   


