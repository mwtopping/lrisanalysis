import numpy as np
from stack_LRIS import *
from math import isclose
from popModel import *
import sys
from tqdm import tqdm
from read_spectrum import *



# define all of the possible population parameters
ALLZS = np.array([1e-5,1e-4,1e-3, 0.002,0.003,0.004,0.006,0.008,0.01,0.014,0.02,0.03,0.04])
ALLZS_str = np.array(['em5','em4','001','002','003','004','006','008','010','014','020','030','040'])
ALLAGES = np.logspace(6,11,51, endpoint=True)


def get_ranges(minz, maxz, minage, maxage):
    # get the index for min metallicity
    index = np.abs(ALLZS - minz).argmin()
    if ALLZS[index] > minz:
        index -= 1
    minzindex = index

    # get the index for max metallicity
    index = np.abs(ALLZS - maxz).argmin()
    if ALLZS[index] < maxz:
        index += 1
    maxzindex = index
 
    # get the index for min age
    index = np.abs(ALLAGES - minage).argmin()
    if ALLAGES[index] > minage:
        index -= 1
    minageindex = index

    # get the index for the max age
    index = np.abs(ALLAGES - maxage).argmin()
    if ALLAGES[index] < maxage:
        index += 1
    maxageindex = index

    return ALLZS[minzindex:maxzindex+1], ALLAGES[minageindex:maxageindex+1]

def get_zname(z):
    for ii, x in enumerate(ALLZS):
        if isclose(z, x, rel_tol=1e-6):
            return ALLZS_str[ii]

    raise ValueError("No models with that metallicity")

#read in the different models
def read_models(Zs, binary='bin', imf='chab', imflim='100'):
    print("Reading in models from file")
    models = []
    for z in tqdm(Zs):
        model = PopModel(binary, imf, imflim, get_zname(z))
        model.load_data()
        model.make_consts()
        models.append(model)
    return models
    
def run_grid(minz, maxz, minage, maxage, minE, maxE, dE, norm):
    Zs, ages = get_ranges(minz, maxz, minage, maxage)
    if dE == 0:
        Es = [minE]
    else:
        Es = np.arange(minE, maxE, dE)
    preview_grid(Zs, ages)
    grid = read_models(Zs)

    # loop through all of the metalicities
    for model in grid:
        # loop through all of the ages
        for age in ages:
            # loop through all of the E(B-V)
            for E in Es:
                model.plot_const(age, norm, redden=E)

    plt.xlim([900, 2000])

# this is just for testing
def get_spectra():
    obj1 = '18543'
    mask1 = 'ae_l3'
    objs = ['16545','11530', '4156', '4711', '22931','40768', '46938', '3666', '3626', '30053', '18543', '25817', '33942','19439', '6283', '14957', '37226','11968', '22939', '4078', '28659','24020', '27216', '27120', '38559', '29834', '32354', '23409', '3668', '22669', '22669','19985', '23344', '23869', '36257', '10494', '12476', '26557', '26557', '39897', '35262','39713', '28846', '8515','4497', '12345','17038', '42363', '25142', '30074', '17437', '6311', '28421', '27627','1740', '21955', '35705', '10066', '20924', '28710', '13364', '5571', '20924','9801', '6963', '16547', '20062', '8232', '32638', '29650','40679', '3324', '21279', '23040', '6569', '9148', '34308','10471', '14739', '14739', '33768','3974', '17204', '17204', '33801']
    #objs = ['41547']


    ws, spectrum = stack_spectra(objs, 0.6, plotting=False)
    spectrum_sig = []

    return ws, spectrum, spectrum_sig

def plot_spectrum(ws, spectrum):
    plot_hist(ws, spectrum, 'k')

# plot an array as a histogram.  Useful for data like spectra
def plot_hist(xs, ys, color, **kwargs):
    # loop through each of the x values, and plot some vertical and horizontal lines to make up a histogram
    for ii in range(len(xs) - 2):
        # this is half of a bin, and is the amount shifted over
        dx = (xs[ii + 2] - xs[ii + 1]) / 2.
        plt.plot([xs[ii + 1] - dx, xs[ii + 1] - dx], [ys[ii], ys[ii + 1]], color=color, **kwargs)
        plt.plot([xs[ii + 1] - dx, xs[ii + 2] - dx], [ys[ii + 1], ys[ii + 1]], color=color, **kwargs)

def preview_grid(zs, ages):
    print("Running grid with values (Z,log[age]):")
    for age in ages:
        sys.stdout.write('-'*(len(zs)*10+1))
        print()
        sys.stdout.write('| ')
        for z in zs:
            sys.stdout.write('{},{} | '.format(get_zname(z), np.log10(age)))
        sys.stdout.write('\n')




if __name__ == "__main__":
    ws, spectrum, spectrum_sig = get_spectra()
    plot_spectrum(ws, spectrum)

    # find normalization factor
    lindex = np.abs(ws - (1560 - 50)).argmin()
    rindex = np.abs(ws - (1600 + 50)).argmin()
    
    norm = np.median(spectrum[lindex:rindex])

    run_grid(0.001, 0.02, 1e8, 1e8, 0.6, 0.6, 0.0, norm)
    plt.xlim([1500, 1600])
    draw_mask(mask1)
    plt.show()




















