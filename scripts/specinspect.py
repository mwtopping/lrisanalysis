import numpy as np
from pathlib import Path
import math
import matplotlib.pyplot as plt
from matplotlib import rc
import astropy.io.fits
from matplotlib.backends.backend_pdf import PdfPages
from popModel import PopModel

# change plotting parameters
rc('font', **{'serif': ['Computer Modern'], 'size': 16})
rc('text', usetex=True)

LAMBDA_MID = 5000


# INPUT:  filename - fits file of spectrum
# OUTPUT: data  - data array containing the spectrum data
#         CRVAL - wavelength of the first pixel
#         CDELT - wavelength spacing of the data
#         naps  - number of apertures in the file
def read_spectrum(filename):
    # read the data into a buffer
    buf = astropy.io.fits.getdata(filename)
    ap=1

    # check if the data has more than one aperture
    if len(np.array(buf).shape) > 1:
        data = buf
        naps = np.array(buf).shape[0]
    else:
        data = buf
        naps = 1
    header = astropy.io.fits.getheader(filename)

    # read in the wavelength solution if there is only one aperture
    if 'CRVAL1' in header.keys():
        CRVAL = header['CRVAL1'] #zero point
        CDELT = header['CDELT1']

    # wavelength solution if there are more than one apertures
    else:
        
        sol = header["WAT2_00{}".format(ap)].strip()
    
        sol = sol.split('{} {} 0'.format(ap, ap))
        sol = sol[-1].strip().split(" ")

        CRVAL=float(sol[0])
        CDELT=float(sol[1])

    return data, CRVAL, CDELT, naps


# read in the spectra from the red and blue side of lris
# INPUT: bfilename - filename of the blue side spectrum
#        rfilename - filename of the red side spectrum
# OUTPUT: ws_b      - wavelength array for the blue side spectrum
#         spectra_b - data for the blue side spectrum
#         ws_r      - wavelength array for the red side spectrum
#         spectra_r - data array for the red side spectrum
def get_spectra(bfilename, rfilename):
    # make sure the blue side spectrum exists
    try:
        data, CRVAL, CDELT, naps = read_spectrum(bfilename)
        ws_b = CRVAL + CDELT*np.arange(len(data))
        spectra_b = data
    except FileNotFoundError:
        print("Missing {}".format(bfilename))
        ws_b = []
        spectra_b = []

    # make sure the red side spectrum exists
    try:
        data, CRVAL, CDELT, naps = read_spectrum(rfilename)
        ws_r = CRVAL + CDELT*np.arange(len(data))
        spectra_r = data
    except FileNotFoundError:        
        print("Missing {}".format(rfilename))
        ws_r = []
        spectra_r = []


    return ws_b, spectra_b, ws_r, spectra_r



# for lris spectra, give the red and blue side, and cut them at the dichroic and concatenate them together
def splice_spectrum(ws_b, spectra_b, ws_r, spectra_r):

    # check to make sure that at least one side of the spectra is available
    if len(spectra_b) == 0 and len(spectra_r) == 0:
        print("No spectra available")
        raise FileNotFoundError

    # this happens if there is only a red side spectrum
    elif len(spectra_b) == 0:

        # cut the red side spectrum at the dichroic wavelength
        rindex = np.array([x for x in ws_r - LAMBDA_MID if x < 0]).argmax()+1 # index to the right of the middle
        ws = ws_r[rindex:] 
        spectrum = spectra_r[rindex:]

    # this happens if there is only a blue side spectrum
    elif len(spectra_r) == 0:

        # cut the blue side at the dichroic spectrum
        bindex = np.array([x for x in ws_b - LAMBDA_MID if x < 0]).argmax() # index to the left of the middle
        ws = ws_b[:bindex] 
        spectrum = spectra_b[:bindex]


    # if both red and blue side spectra exist
    else:

        # this will check to make sure that the dichroic cutof is within the spectra somewhere
        #  otherwise, it will not cut the array at all
        try:
            rindex = np.array([x for x in ws_r - LAMBDA_MID if x < 0]).argmax()+1 # index to the right of the middle
        except ValueError:
            rindex = 0
        try:
            bindex = np.array([x for x in ws_b - LAMBDA_MID if x < 0]).argmax() # index to the left of the middle
        except ValueError:
            bindex = 0

        # combine the two spectra
        ws = np.append(ws_b[:bindex], ws_r[rindex:])
        spectrum = np.append(spectra_b[:bindex], spectra_r[rindex:])

    return ws, spectrum


# definition of all data locations and identifiers
def load_data():
    objs = {}
    locs = {}
    objs['gs_l1'] = ['31791','31344','32837','36705','40768','37988','31854','33248','41547','45188','35178','40679','42556','39198','41218','41886','35705','46335','34114','38119','42363','38559','45180','46938','35779','42809','45531','39713','40218'    ,'38116']
    locs['gs_l1'] = "/Users/michaeltopping/lrisData/gs_l1-combine/"

    objs['co_l1'] = ['11968','11530','11443','11153','10056','10143','9801','9094','8540','8338','8081','7912','7273','6963','6826','6417','5912','5686','5462','4945','4154','3694','3112','2672','2786','2207','1908','1740','1382','964','541','307','241    ']
    locs['co_l1'] = "/Users/michaeltopping/lrisData/lris_jan17/co_l1/"

    objs['co_l2'] = ['16545','16547','17038','17233','18067','19985','20062','19439','19712','21743','20171','21780','22939','21955','23134','23663','23183','23210','23841','24427','24020','24414','24053','24738','26073','25322','27216','27120','26332'    ,'28258','27906']
    locs['co_l2'] = "/Users/michaeltopping/lrisData/lris_jan17/co_l2/"

    objs['gn_l1']=['30053','32526','29743','26621','30461','31955','30709','28846','29834','21772','23344','27035','23869','18128','25142','21279','17958','21845','22023','20924','16713','19654','18161','15186','12980','12157','10596','12345','10645']
    locs['gn_l1'] = "/Users/michaeltopping/lrisData/lris_apr17/gn_l1/"

    objs['ae_l1']=['30074','36257','36451','33808','32354','24481','38356','23409','34661','23040','28710','21675','17437','16496','16730','15082','25522','16121','14957','26153','12918','17085','10471','10494','3668','6569','11729','14880','4711','631    1','3112']
    locs['ae_l1']= "/Users/michaeltopping/lrisData/lris_apr17/ae_l1/"

    objs['co_l5']=['4078','4497','3666','6018','4446','3974','6379','3626','7430','7735','3185','6283','851','6179','5107','7883','3324','4029','10066','10085','4441'    ,'3757','8697','4156','5162','6743']
    locs['co_l5'] = "/Users/michaeltopping/lrisData/lris_jan2018/co_l5/"

    objs['co_l6']=['13101','13299','15710','12476','11716','13364','12577','12611','14423','10235','11343','10280','9044','10835','9148','10093','8679','8515','6413',    '9251','5814','8232','6817','5901','5571','4962']
    locs['co_l6'] = "/Users/michaeltopping/lrisData/lris_jan2018/co_l6/"

    objs['gn_l3'] = ['11892','14739','17940','17204','22235','17667','19067','19350','16351','17714','17530','    22299','25498','24825','24388','22669','24846','25505','22487','23674','25688','28237','28599','26557','29095','24328']
    locs['gn_l3'] = "/Users/michaeltopping/lrisData/gn_l3-combine/"

    objs['ae_l3']=['33768','28659','34308','31226','33801','22931','20924','39897','30278','40851','28421','27    825','18543','33973','34813','32638','34848','37226','32761','24341','27627','35262','25817','29650','33942']
    locs['ae_l3'] = "/Users/michaeltopping/lrisData/ae_l3-combine/"

    return locs, objs


# plot a spectrum as a histogram
def plot_hist(ax, xs, ys, color, **kwargs):

    plt.sca(ax)

    # loop through each of the x values, and plot some vertical and horizontal lines to make up a histogram
    for ii in range(len(xs)-2):
        plt.plot([xs[ii+1]-.5, xs[ii+1]-.5], [ys[ii], ys[ii+1]], color=color, **kwargs)
        plt.plot([xs[ii+1]-.5, xs[ii+2]-.5], [ys[ii+1], ys[ii+1]], color=color, **kwargs)

def plot_region(fnameb, fnamer, fnamebsig, fnamersig, z, wavelength, window, ax, obj):
    ws_b, spectra_b, ws_r, spectra_r = get_spectra(fnameb, fnamer)
    ws, spectrum = splice_spectrum(ws_b, spectra_b, ws_r, spectra_r)

    ws_bsig, spectra_bsig, ws_rsig, spectra_rsig = get_spectra(fnamebsig, fnamersig)
    ws_sig, spectrum_sig = splice_spectrum(ws_bsig, spectra_bsig, ws_rsig, spectra_rsig)


    # find normalization factor
    lindex = np.abs(ws - (1450*(1+z) - 50)).argmin()
    rindex = np.abs(ws - (1450*(1+z) + 50)).argmin()

    norm = np.median(1e29*spectrum[lindex:rindex])
    
    # cut out a window surrouding the line of interest
    lindex = np.abs(ws - (wavelength*(1+z) - window)).argmin()
    rindex = np.abs(ws - (wavelength*(1+z) + window)).argmin()

    # normalize the spectra to order unity to avoid floating point calculation errors
    subws = ws[lindex:rindex] / (1+z)
    subspectrum = 1e29*spectrum[lindex:rindex]
    subspectrumsig = 1e29*spectrum_sig[lindex:rindex]
    


    ax.fill_between(subws, subspectrum-subspectrumsig, subspectrum+subspectrumsig, alpha=0.4)
    plot_hist(ax, subws, subspectrum, color='#1f77b4')
    plt.xlim([(wavelength-window/(1+z)), (wavelength+window/(1+z))])







def inspect_region(wavelength, window):

    masks =['gs_l1', 'co_l1', 'co_l2', 'gn_l1', 'ae_l1', 'co_l5', 'co_l6', 'gn_l3', 'ae_l3'] 
    
    # load in the data
    locs, objs = load_data()


    # get a master dictionary of objects and their estimated/already measured redshifts
    cat_mosdef = np.genfromtxt('/Users/michaeltopping/Documents/mosdef_zcat.final.dat', usecols=(2, 5))
    mosdef = {} 
    for row  in cat_mosdef:
        mosdef["{}".format(int(row[0]))] = row[1]

    addtl_zs = np.genfromtxt('morezs.init')
    for row in addtl_zs:
        mosdef["{}".format(int(row[0]))] = row[1]

    with PdfPages('CIV.pdf') as pdf:
        for mask in masks:

            # get the data location
            loc = locs[mask]

            Nobjs = len(objs[mask])

            ncols = 4
            nrows = math.ceil(Nobjs/float(ncols))

            
            fig, axs = plt.subplots(nrows, ncols, figsize=(8, 8))

            for ii, obj in enumerate(objs[mask]):
                # assemble the filenames
                fnameb = loc+"b/split/{}.b.{}.msdfc_vs.fits".format(mask, obj)
                fnamer = loc+"r/split/{}.r.{}.msdfc_vs.fits".format(mask, obj)
                fnamebsig = loc+"b/split/{}.bsig.{}.msdfc_vs.fits".format(mask, obj)
                fnamersig = loc+"r/split/{}.rsig.{}.msdfc_vs.fits".format(mask, obj)


                if obj in mosdef:
                    z = mosdef[obj]

                    if z > 0:
                
                        try:
                            ax = axs[int(ii/ncols),ii%ncols]
                            plot_region(fnameb, fnamer, fnamebsig, fnamersig, z, wavelength, window, ax, obj)
                        except FileNotFoundError:
                            print("No spectra available for this object")


                axs[int(ii/ncols),ii%ncols].text(0.95, 0.1,obj,verticalalignment='center', horizontalalignment='right',
                             transform = axs[int(ii/ncols),ii%ncols].transAxes, fontsize=8) 





            for ax in axs.flatten():
                ax.yaxis.get_offset_text().set_visible(False)
                ax.xaxis.get_offset_text().set_visible(False)
                plt.setp(ax.get_yticklabels(), visible=False)
                plt.setp(ax.get_xticklabels(), visible=False)

            plt.suptitle("{} CIV".format(mask.replace('_','\_')))
            pdf.savefig()
            plt.close()
    
        


def c4_sample():
    fig, axs = plt.subplots(1, 4, figsize=(12,3.5))

    bnames = []
    zs = [2.1411, 2.0969, 2.1877, 2.1387]
    names = ["GOODSS-42363","COSMOS-11530", "AEGIS-3668", "AEGIS-18543"]

    bnames.append("/Users/michaeltopping/lrisData/gs_l1-combine/b/split/gs_l1.b.42363.msdfc_vs.fits")
    bnames.append("/Users/michaeltopping/lrisData/lris_jan17/co_l1/b/split/co_l1.b.11530.msdfc_vs.fits")
    bnames.append("/Users/michaeltopping/lrisData/lris_apr17/ae_l1/b/split/ae_l1.b.3668.msdfc_vs.fits")
    bnames.append("/Users/michaeltopping/lrisData/ae_l3-combine/b/split/ae_l3.b.18543.msdfc_vs.fits")

    wavelength=1550
    window = 50

    for ii, bname in enumerate(bnames):
        ws, data, jung, junk2 = get_spectra(bname, bname)
        z = zs[ii]

        # cut out a window surrouding the line of interest
        lindex = np.abs(ws - (1450*(1+z))).argmin()
        rindex = np.abs(ws - (1500*(1+z))).argmin()

        subspectrum = data[lindex:rindex]
        norm = np.average(subspectrum)



        # cut out a window surrouding the line of interest
        lindex = np.abs(ws - (wavelength*(1+z) - window)).argmin()
        rindex = np.abs(ws - (wavelength*(1+z) + window)).argmin()

        # normalize the spectra to order unity to avoid floating point calculation errors
        subws = ws[lindex:rindex] / (1+z)
        subspectrum = data[lindex:rindex]

        plot_hist(axs[ii], subws, subspectrum/norm, 'k')
        axs[ii].text(0.03, 0.89,'$z={}$'.format(zs[ii]),verticalalignment='center', horizontalalignment='left',transform = axs[ii].transAxes, fontsize=11)
        axs[ii].text(0.03, 0.95,'{}'.format(names[ii]),verticalalignment='center', horizontalalignment='left',transform = axs[ii].transAxes, fontsize=11)
        axs[ii].locator_params(axis='y', nbins=4, tight=True)
    axs[0].set_ylabel(r'Normalized $F_{\nu}$')
    axs[1].locator_params(axis='y', nbins=5)
    fig.text(0.53, 0.025, r'Rest Wavelength $[\rm \AA]$', ha='center')
#    axs[-1].set_ylim([-.4, 5.8])
    axs[0].set_ylim([0, 1.4])
    axs[1].set_ylim([0, 1.4])
    axs[2].set_ylim([0, 1.4])
    axs[3].set_ylim([0, 1.4])
    plt.tight_layout(pad=1.1)
    plot_bpass(axs)
    plt.savefig("../figures/CIV_sample_bpass-const_age.pdf")


def plot_bpass(axs):
    zs = ['020', '010', '008', '004', '002']
    ages = [5e6, 1e7, 5e7, 1e8, 3e8]
    model = PopModel('bin', 'chab', '100', '010')
    for ax in axs:
        for age in ages:
            model.load_data()
            nws, nspectrum = model.get_piece_const(1450, 1500, age)
            ws, spectrum = model.get_piece_const(1535, 1565, age)
            ax.plot(ws, spectrum/np.average(nspectrum))



def read_flags(filename):
    flags = {}
    flagfile = Path(filename)
    with flagfile.open() as f:
        print("sdfas")
        for line in f:
            print("line",line)
            line = line.strip().split()

            ID = line[1]
            flag = line[2]
            flags[ID] = flag
    return flags


def main():
#    inspect_region(1550, 100)
    c4_sample()

if __name__ == "__main__":
    main()
