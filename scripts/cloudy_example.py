import numpy as np
import matplotlib.pyplot as plt

c = 3e10
h = 4.135e-15

data = np.genfromtxt('bpasschab100_zstar0.008_zneb-0.6000000000000003_age8.20_continuum.dat', usecols=(0,1,3,8))


wavelengths = data[:,0]
incident = data[:,1]
outgoing = data[:,2]
lines = data[:,3]

print(wavelengths)

plt.plot(wavelengths, incident)
#plt.plot(wavelengths, outgoing, linewidth=1)
#plt.plot(wavelengths, lines, linewidth=1)
plt.plot(wavelengths, outgoing - lines)
plt.plot(wavelengths, incident + outgoing - lines, 'k')

plt.xlim([700,3000])
plt.show()



