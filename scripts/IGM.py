import numpy as np
import matplotlib.pyplot as plt
from cosmology import *




Lyman = [1215.67]

def f(z):
    A = 400
    z1 = 1.2
    z2 = 4
    g1 = 0.2
    g2 = 2.5
    g3 = 4.0

    if z < z1:
        return A*((1+z)/(1+z1))**g1
    elif z < z2:
        return A*((1+z)/(1+z1))**g2
    else:
        return A*((1+z2)/(1+z1))**g2 * ((1+z)/(1+z2))**g3


# the probability of finding an absorber at z=z+dz given an absorber at z
def prob_z(z, dz):
    return f(z)*np.exp(-1*f(z)*dz)



def draw_dz(z):
    zs = np.linspace(0, 0.1, 1000)
    ps = [prob_z(z, iz) for iz in zs]
    return np.random.choice(zs, p=ps/np.sum(ps))


def prob_NH(N):
    Nl = 10**12.0
    Nc = 10**17.2
    Nu = 10**22.0
    beta1 = 1.6
    beta2 = 1.3


    if Nl < N < Nc:
        return (N/Nc)**(-beta1)
    elif Nc < N < Nu:
        return (N/Nc)**(-beta2)
    else:
        return 0

def draw_NH():
    Ns = np.logspace(12, 22, 1000)
    ps = [prob_NH(NH) for NH in Ns]
    return np.random.choice(Ns, p=ps/np.sum(ps))



def transmission(zem):

    zlist = []
    z = 0
    while z < zem:
        nextz = draw_dz(z)
        zlist.append(z+nextz)
        z += nextz

    return zlist


def test():

    z = 2.4
    zs = transmission(z)
    Ns = []
    print(len(zs))
    for ii in range(len(zs)):
        Ns.append(draw_NH())
    for z in zs:
        plt.plot([z,z], [0, 1], 'k', alpha=0.1)
    plt.ylim([0,2])

    plt.figure()
    plt.hist(np.log10(np.array(Ns)))
    plt.show()

if __name__ == "__main__":
    test()

