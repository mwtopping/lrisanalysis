import astropy.io.fits
from tqdm import tqdm
import numpy as np





def read_data(filename):

    f = astropy.io.fits.open(filename)
    data = f[1]


    return data

def get_column(filename, field):
    
    data = read_data(filename)

    col = []

    for row in tqdm(data.data[:1000]):
        col.append(row.field(field))


    return np.array(col)


if __name__ == "__main__":
    print(get_column('../data/gal_line_dr7_v5_2.fit', 'OIII_5007_FLUX'))
    print(get_column('../data/gal_line_dr7_v5_2.fit', 'H_BETA_FLUX'))
    print(get_column('../data/gal_line_dr7_v5_2.fit', 'NII_6584_FLUX'))
    print(get_column('../data/gal_line_dr7_v5_2.fit', 'H_ALPHA_FLUX'))

