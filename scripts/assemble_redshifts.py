import numpy as np
import pathlib


# list of lines that we measured and are in the redshift catalog
linenames = ["SiIIa", "OI+SiII", "CII", "SiIIb", "FeII", "AlII", "LyA"]

# read in the redshift data from the catalogs
# INPUT: cats - a filename, or list of filenames of the redshift data
def read_catalogs(cats):
    
    # create the dictionary to store data
    redshifts = {}

    # check if either a single catalog, or a list of them were given
    if not isinstance(cats, list):
        cats = [cats]

    # loop through each catalog, even if there is only 1
    for cat in cats:
    
        # open up and loop through each line of the file
        catfile = pathlib.Path("../data/{}".format(cat))
        with catfile.open() as f:
            for line in f:
                # exclude comments
                if line[0] == '#':
                    pass
                else:
                    # convert the file to a list of numbers
                    line = list(filter(None, line.strip().split(' ')))
                    ID = line[0]
            
                    # loop through each line and take care of 'nan' entries
                    for ii, val in enumerate(line):
                        if val == 'nan':
                            line[ii] = '-2.000'
        
                    # fill the dictionary with the values from the file
                    redshifts[ID] = assign_zs(ID, line)
                    redshifts[ID]["mask"] = cat.split('.')[0]

    return redshifts

        
    

# look through the line in the catalog and assign values to create the data structure
#  for each line and their error
def assign_zs(ID, line):

    dic = {}
    dic["SiIIa"]=float(line[1])
    dic["dSiIIa"]=float(line[2])

    dic["OI+SiII"]=float(line[3])
    dic["dOI+SiII"]=float(line[4])

    dic["CII"]=float(line[5])
    dic["dCII"]=float(line[6])

    dic["SiIIb"]=float(line[7])
    dic["dSiIIb"]=float(line[8])

    dic["FeII"]=float(line[9])
    dic["dFeII"]=float(line[10])
              
    dic["AlII"]=float(line[11])
    dic["dAlII"]=float(line[12])

    dic["LyA"]=float(line[13])
    dic["dLyA"]=float(line[14])
 
    return dic 


# get a final absorption redshift from the set of available of individual redshifts
def get_final_zabs(ID, zs):

    # create an array that tells which lines are usable
    usable = np.zeros(int(len(zs) / 2))

    # for lines that have been measured, set them to be 'usable'
    for ii, line in enumerate(linenames):
        if zs[line] > 0:
            usable[ii] = 1
    
    # loop through the 3 'good' lines
    ztot = 0
    ws = 0
    for l in [0, 2, 3]:
        line = linenames[l]
        lineerr = 'd'+line
        if usable[l]:
            try:
                ztot += zs[line] / (zs[lineerr]**2)
            except ZeroDivisionError:
                print(ID)
                exit()
            ws += (1 / zs[lineerr]) ** 2
    # if at least one of the 3 preferred lines are available calculate the weighted mean
    if ztot > 0:
        z = ztot/ws
        zerr = 1 / np.sqrt(ws)
    # if none of the preferred lines were available
    else:
        # try alII
        if usable[5]:
            z = zs[linenames[5]] 
            zerr = zs['d'+linenames[5]] 
        # try oI + SiII
        elif usable[1]:
            z = zs[linenames[1]] 
            zerr = zs['d'+linenames[1]] 
        # no luck
        else:
            z = -2.000
            zerr = -2.000

    return z, zerr


# the main function that loops through each object to get their redshift
def main():
    # read in the data from each catalog given here
    zs = read_catalogs(["gs_l1.z.cat", "co_l1.z.cat", "co_l2.z.cat", "gn_l1.z.cat", "ae_l1.z.cat", "co_l5.z.cat", "co_l6.z.cat", "gn_l3.z.cat", "ae_l3.z.cat"])
    # print the header of the table
    print("#    ID         zabs  zabs_err  zLyA  zLyAerror")
    # loop through each object that was read in
    for obj in zs.keys():
        # get the absorption redshift and its error
        zabs, zabserr = get_final_zabs(obj, zs[obj])

        # print out the redshift data
        print("{}.{:7s} {:7.4f} {:7.4f} {:7.4f} {:7.4f}".format(
                zs[obj]["mask"], obj, zabs, zabserr, zs[obj]["LyA"], zs[obj]["dLyA"]))


if __name__ == "__main__":
    main()





