import numpy as np

# physical constans
c = 3e10

def pDistance(zmin, zmax, Omegam=0.3, OmegaL=0.3, H0=70):
    zs = np.linspace(zmin, zmax, 1000)

    # convert H0 to other units
    H0 = H0 / 3.086e19

    Hz = H0 / np.sqrt(OmegaL + Omegam*(1+zs)**3)

    integrand = 1 / (1+zs) * c / Hz

    result = np.trapz(integrand, x=zs)


    # return the result in mpc
    return result / 3.086e24





if __name__ == "__main__":
    print(pDistance(0, 1))
