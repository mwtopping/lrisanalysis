import matplotlib.pyplot as plt
from tqdm import tqdm
from numba import jit, njit
import cProfile
import time
import os
import pprint
import numpy as np
import astropy.io.fits
from matplotlib import rc
from collections import OrderedDict
from scipy import optimize
import warnings
import multiprocessing as mp
# change plotting parameters
rc('font', **{'serif': ['Computer Modern'], 'size': 16})
rc('text', usetex=True)

# constants to define the lris dichroic wavelength and the size of the window that will be used in the fit
LAMBDA_MID = 5000
FIT_WINDOW = 50

# define a dictionary of lines that we would like to fit
linelist = OrderedDict({"SiIIa":1260.4221, "OI+SiII":1303.26935, "CII":1334.5323, "SiIIb":1526.70698,"FeII":1608.45085, "AlII":1670.7886})
LyA = 1215.67

# make the processes lower priority so it doesnt destroy your computer
#os.nice(20)

# define an absorption line as a linear function plus a gaussian
def abs(x, *args):
    a, b, d, s, mu = args
    if s == 0:
        return a + b*x
    else:
        return a + b*x + d / (s * np.sqrt(2*np.pi)) * np.exp(-0.5*( (x-mu) / s**2)**2)


# define an emission line as a linear function plus a gaussian
@jit
def em(x, *args):
    a, b, d, s, mu = args
    if s == 0:
        return a + b*x
    else:
        return a + b*x +  d / (s * np.sqrt(2*np.pi)) * np.exp(-0.5*( (x-mu) / s**2)**2)


# INPUT:  filename - fits file of spectrum
# OUTPUT: data  - data array containing the spectrum data
#         CRVAL - wavelength of the first pixel
#         CDELT - wavelength spacing of the data
#         naps  - number of apertures in the file
def read_spectrum(filename):
    # read the data into a buffer
    buf = astropy.io.fits.getdata(filename)
    ap=1

    # check if the data has more than one aperture
    if len(np.array(buf).shape) > 1:
        data = buf
        naps = np.array(buf).shape[0]
    else:
        data = buf
        naps = 1
    header = astropy.io.fits.getheader(filename)

    # read in the wavelength solution if there is only one aperture
    if 'CRVAL1' in header.keys():
        CRVAL = header['CRVAL1'] #zero point
        CDELT = header['CDELT1']

    # wavelength solution if there are more than one apertures
    else:
        
        sol = header["WAT2_00{}".format(ap)].strip()
    
        sol = sol.split('{} {} 0'.format(ap, ap))
        sol = sol[-1].strip().split(" ")

        CRVAL=float(sol[0])
        CDELT=float(sol[1])

    return data, CRVAL, CDELT, naps



# read in the spectra from the red and blue side of lris
# INPUT: bfilename - filename of the blue side spectrum
#        rfilename - filename of the red side spectrum
# OUTPUT: ws_b      - wavelength array for the blue side spectrum
#         spectra_b - data for the blue side spectrum
#         ws_r      - wavelength array for the red side spectrum
#         spectra_r - data array for the red side spectrum
def get_spectra(bfilename, rfilename):
    # make sure the blue side spectrum exists
    try:
        data, CRVAL, CDELT, naps = read_spectrum(bfilename)
        ws_b = CRVAL + CDELT*np.arange(len(data))
        spectra_b = data
    except FileNotFoundError:
        print("Missing {}".format(bfilename))
        ws_b = []
        spectra_b = []

    # make sure the red side spectrum exists
    try:
        data, CRVAL, CDELT, naps = read_spectrum(rfilename)
        ws_r = CRVAL + CDELT*np.arange(len(data))
        spectra_r = data
    except FileNotFoundError:        
        print("Missing {}".format(rfilename))
        ws_r = []
        spectra_r = []


    return ws_b, spectra_b, ws_r, spectra_r


# given a spectrum and the error spectrum, perturb it assuming zero mean gaussian noise
# INPUT: spectrum     - the data spectrum
#        spectrum_sig - the error spectrum
# OUTPUT: newspectrum - the perturbed spectrum
def perturb_spectrum(spectrum, spectrum_sig):

    newspectrum = []

    # loop through each data point and corresponding error point, create noise and add them
    for s, sig in zip(spectrum, spectrum_sig):
        noise = np.random.normal(0, sig)
        newspectrum.append(s+noise)

    
    return newspectrum


# for lris spectra, give the red and blue side, and cut them at the dichroic and concatenate them together
def splice_spectrum(ws_b, spectra_b, ws_r, spectra_r):

    # check to make sure that at least one side of the spectra is available
    if len(spectra_b) == 0 and len(spectra_r) == 0:
        print("No spectra available")
        raise FileNotFoundError

    # this happens if there is only a red side spectrum
    elif len(spectra_b) == 0:

        # cut the red side spectrum at the dichroic wavelength
        rindex = np.array([x for x in ws_r - LAMBDA_MID if x < 0]).argmax()+1 # index to the right of the middle
        ws = ws_r[rindex:] 
        spectrum = spectra_r[rindex:]

    # this happens if there is only a blue side spectrum
    elif len(spectra_r) == 0:

        # cut the blue side at the dichroic spectrum
        bindex = np.array([x for x in ws_b - LAMBDA_MID if x < 0]).argmax() # index to the left of the middle
        ws = ws_b[:bindex] 
        spectrum = spectra_b[:bindex]


    # if both red and blue side spectra exist
    else:

        # this will check to make sure that the dichroic cutof is within the spectra somewhere
        #  otherwise, it will not cut the array at all
        try:
            rindex = np.array([x for x in ws_r - LAMBDA_MID if x < 0]).argmax()+1 # index to the right of the middle
        except ValueError:
            rindex = 0
        try:
            bindex = np.array([x for x in ws_b - LAMBDA_MID if x < 0]).argmax() # index to the left of the middle
        except ValueError:
            bindex = 0

        # combine the two spectra
        ws = np.append(ws_b[:bindex], ws_r[rindex:])
        spectrum = np.append(spectra_b[:bindex], spectra_r[rindex:])

    return ws, spectrum


# fitting procedure for lyman alpha
# INPUT: ws           - wavelength array for the spectrum
#        spectrum     - data array
#        spectrum_sig - error spectrum
#        z            - estimated redshift of the object
#        fitcat       - catalog of the fitting parameters that we are saving
# OUTPUT: None
# This will run the fitting procedure a number of times, perturbing the spectrum by the
#  noise for each one.  It will then write the best fit parameters to a file
def fit_lya(ws, spectrum, spectrum_sig, z, fitcat):
    # cut out the spectrum surrouding the lyman alpha line
    lindex = np.abs(ws - (LyA*(1+z) - (FIT_WINDOW-15))).argmin()
    rindex = np.abs(ws - (LyA*(1+z) + (FIT_WINDOW-15))).argmin()

    # the spectra are adjusted to be near unity to prevent floating point errors
    subws = ws[lindex:rindex]
    subspectrum = 1e29*spectrum[lindex:rindex]
    subspectrum_sig = 1e29*spectrum_sig[lindex:rindex]

    fitcat.write('LyA{}\n'.format(LyA))

    # number of monte carlo iterations to run
    niter = 100

    for ii in range(niter):

        # get a newly perturbed spectrum
        pspectrum = perturb_spectrum(subspectrum, subspectrum_sig)
             
        # define the likelyhood that we want to minimize
        def likelyhood(args):
            return np.sum( ((em(subws, *args) - pspectrum)/subspectrum_sig)**2)

        # this is what actually does the fitting.  the basinhopping algorith usually does a better job,
        #  but is slower
        fit = optimize.basinhopping(likelyhood, [np.median(subspectrum), 0,  5, 1, np.median(subws)], niter=20)
#        fit = optimize.minimize(likelyhood, [np.median(subspectrum), 0,  5, 1, np.median(subws)])

        # write the best fit parameters to the catalog
        [fitcat.write("{} ".format(x)) for x in fit.x]
        fitcat.write('\n')

   


# fitting procedure for fitting an absorption line
# INPUT: ws           - wavelength array for the spectrum
#        spectrum     - data array
#        spectrum_sig - error spectrum
#        z            - estimated redshift of the object
#        fitcat       - catalog of the fitting parameters that we are saving
# OUTPUT: None
def fit_abs_line(ws, spectrum,spectrum_sig, w, z, fitbuffer, line):


    # cut out a window surrouding the line of interest
    lindex = np.abs(ws - (w*(1+z) - FIT_WINDOW)).argmin()
    rindex = np.abs(ws - (w*(1+z) + FIT_WINDOW)).argmin()

    # normalize the spectra to order unity to avoid floating point calculation errors
    subws = ws[lindex:rindex]
    subspectrum = 1e29*spectrum[lindex:rindex]
    subspectrum_sig = 1e29*spectrum_sig[lindex:rindex]

    # number of iterations to run
    niter = 100

    for ii in range(niter):

        # get a newly perturbed spectrum
        pspectrum = perturb_spectrum(subspectrum, subspectrum_sig)

        def likelyhood(args):
            return np.sum( ((abs(subws, *args) - pspectrum))**2)
            #return np.sum( ((abs(subws, *args) - pspectrum)/subspectrum_sig)**2)

   #     print([np.median(pspectrum), 0, -1, 1, np.median(subws)])
        # algorithm to use in order to find the best fit parameters
        #  the basinhopping algorithm works the best, but is slow
        fit = optimize.basinhopping(likelyhood, [np.median(pspectrum), 0, -2, 1, np.median(subws)], niter=20)
#        fit = optimize.minimize(likelyhood, [np.median(pspectrum), 0, -1, 1, np.median(subws)])
   #     print(pspectrum)
   #     print(subspectrum_sig)
   #     print(fit)
   #     print(fit.x)
#        plt.plot(subws,subspectrum_sig)
#        plt.ylim([0,2])
 
   
        # write the parameters to the output file
        fitbuffer[line].append(fit.x)
#        [fitcat.write("{} ".format(x)) for x in fit.x]
#        fitcat.write('\n')





# run through all of the spectral lines we want to fit and fit them
# INPUT: ws           - wavelength array for the spectrum
#        spectrum     - data array
#        spectrum_sig - error spectrum
#        z            - estimated redshift of the object
#        fitcat       - catalog of the fitting parameters that we are saving
# OUTPUT: None
def fit_all_abs(ws, spectrum, spectrum_sig, z, fitcat):

    # loop through each line
#    for ii, line in enumerate(linelist):
#        w = linelist[line]
#        fitcat.write('{}{}\n'.format(line, w))        
#        # fit each line
#        fit_abs_line(ws, spectrum,spectrum_sig, w, z, fitcat, line)
#
    manager = mp.Manager()
    fitbuffer = manager.dict()
    for line in linelist:
        fitbuffer[line] = manager.list()
    processes = []
    # testing multithreading
    for ii, line in enumerate(linelist):
        w = linelist[line]
        processes.append(mp.Process(target=fit_abs_line, args=(ws, spectrum, spectrum_sig, w, z, fitbuffer, line))) 
        processes[-1].start()
#    fitbuffer=None
#    for ii, line in enumerate(linelist):
#        
#        w = linelist[line]
#        fit_abs_line(ws, spectrum, spectrum_sig, w, z, fitbuffer, line)


    # wait for threads to finish
    for ii, p in enumerate(processes):
        p.join()


    for line in linelist:
        w = linelist[line]
        fitcat.write('{}{}\n'.format(line, w))
        for x in fitbuffer[line]:
            for val in list(x):
                fitcat.write("{} ".format(val))
            fitcat.write('\n')

        
        


# procedure that will read in all data and fit all of the lines
# INPUT: fnameb    - fiilename of the blue side spectrum
#        fnamer    - filename of the red side spectrum
#        fnamebsig - filename of the blue side error spectrum
#        fnamersig - filename of the red side error spectrum
#        z         - redshift estimate of the object
#        obj       - object identifier
#        mask      - the name of the mask the object lies on
#        fitcat    - catalog that will contain all of the fitting data
def measure_z(fnameb, fnamer, fnamebsig, fnamersig, z, obj, mask, fitcat):
    # this is an array that will control which lines are considered a good fit
    usable = np.ones(7)
    
    print(obj)
    # read in all the spectra and error spectra
    ws_b, spectra_b, ws_r, spectra_r = get_spectra(fnameb, fnamer)
    ws_bsig, spectra_bsig, ws_rsig, spectra_rsig = get_spectra(fnamebsig, fnamersig)

    # put the spectra together
    ws, spectrum = splice_spectrum(ws_b, spectra_b, ws_r, spectra_r)
    ws_sig, spectrum_sig = splice_spectrum(ws_bsig, spectra_bsig, ws_rsig, spectra_rsig)
    # sometimes there is an extra pixel on the end of the error spectra
    if len(spectrum) < len(spectrum_sig):
        spectrum_sig = spectrum_sig[0:len(spectrum)]
    elif len(spectrum) > len(spectrum_sig):
        spectrum = spectrum[0:len(spectrum_sig)]


    # fit all of the absorption lines
    fit_all_abs(ws, spectrum, spectrum_sig, z, fitcat)
    
    # fit lyman alpha
    fit_lya(ws, spectrum, spectrum_sig, z, fitcat)
        



# definition of all data locations and identifiers
def load_data():
    objs = {}
    locs = {}
#    objs['gs_l1'] = ['31791','31344','32837','36705','40768','37988','31854','33248','41547','45188','35178','40679','42556','39198','41218','41886','35705','46335','34114','38119','42363','38559','45180','46938','35779','42809','45531','39713','40218','38116']
    
    locs['gs_l1'] = "/Users/michaeltopping/lrisData/gs_l1-combine/"
    objs['gs_l1'] = ['35178']

#    objs['co_l1'] = ['11968','11530','11443','11153','10056','10143','9801','9094','8540','8338','8081','7912','7273','6963','6826','6417','5912','5686','5462','4945','4154','3694','3112','2672','2786','2207','1908','1740','1382','964','541','307','241']
    objs['co_l1'] = ['3112']
    locs['co_l1'] = "/Users/michaeltopping/lrisData/lris_jan17/co_l1/"

#    objs['co_l2'] = ['16545','16547','17038','17233','18067','19985','20062','19439','19712','21743','20171','21780','22939','21955','23134','23663','23183','23210','23841','24427','24020','24414','24053','24738','26073','25322','27216','27120','26332','28258','27906']
    objs['co_l2'] = ['20062']
    locs['co_l2'] = "/Users/michaeltopping/lrisData/lris_jan17/co_l2/"

#    objs['gn_l1']=['30053','32526','29743','26621','30461','31955','30709','28846','29834','21772','23344','27035','23869','18128','25142','21279','17958','21845','22023','20924','16713','19654','18161','15186','12980','12157','10596','12345','10645']
    objs['gn_l1'] = ['10596']
    locs['gn_l1'] = "/Users/michaeltopping/lrisData/lris_apr17/gn_l1/"

#    objs['ae_l1']=['30074','36257','36451','33808','32354','24481','38356','23409','34661','23040','28710','21675','17437','16496','16730','15082','25522','16121','14957','26153','12918','17085','10471','10494','3668','6569','11729','14880','4711','6311','3112']
    objs['ae_l1'] = ['12918', '6569', '28710']
    locs['ae_l1']= "/Users/michaeltopping/lrisData/lris_apr17/ae_l1/"

#    objs['co_l5']=['4078','4497','3666','6018','4446','3974','6379','3626','7430','7735','3185','6283','851','6179','5107','7883','3324','4029','10066','10085','4441','3757','8697','4156','5162','6743']
    objs['co_l5'] = ['4441', '4029', '4930']
    locs['co_l5'] = "/Users/michaeltopping/lrisData/lris_jan2018/co_l5/"

#    objs['co_l6']=['13101','13299','15710','12476','11716','13364','12577','12611','14423','10235','11343','10280','9044','10835','9148','10093','8679','8515','6413','9251','5814','8232','6817','5901','5571','4962']
    objs['co_l6'] = ['6413']
    locs['co_l6'] = "/Users/michaeltopping/lrisData/lris_jan2018/co_l6/"

#    objs['gn_l3'] = ['21617','11892','14739','17940','17204','22235','17667','19067','19350','16351','17714','17530','22299','25498','24825','24388','22669','24846','25505','22487','23674','25688','28237','28599','26557','29095','24328']
#    objs['gn_l3'] = ['11892','14739','17940','17204','22235','17667','19067','19350','16351','17714','17530','22299','25498','24825','24388','22669','24846','25505','22487','23674','25688','28237','28599','26557','29095','24328']
    objs['gn_l3'] = ['22235', '19067', '17530', '22299', '19530']
    objs['gn_l3'] = ['17940']
    locs['gn_l3'] = "/Users/michaeltopping/lrisData/gn_l3-combine/"

#    objs['ae_l3']=['33768','28659','34308','31226','33801','22931','20924','39897','30278','40851','28421','27825','18543','33973','34813','32638','34848','37226','32761','24341','27627','35262','25817','29650','33942']
    objs['ae_l3'] = ['20924']
    locs['ae_l3'] = "/Users/michaeltopping/lrisData/ae_l3-combine/"

    return locs, objs

# this initializes the procedure to loop through each object and fit all of the lines
def run_fits():
    # array of masks to use
#    masks = ['gs_l1', 'co_l1', 'co_l2', 'gn_l1', 'ae_l1', 'co_l5', 'co_l6', 'gn_l3', 'ae_l3']
#    masks = ['gs_l1', 'co_l1', 'gn_l1', 'ae_l1', 'co_l5', 'co_l6', 'gn_l3', 'ae_l3']
    masks = ['gn_l3']

    # load in object ids and locations
    locs, objs = load_data()
    
    # if you want to start on a different mask
    maskstart = 0

    # this will create the file that will contain all of the fitting parameters
    fitcat = open('fits_more.cat', 'w')

    
    # loop through all of the masks
    for mask in masks[maskstart:]:

        # get the data location
        loc = locs[mask]

        # get a master dictionary of objects and their estimated/already measured redshifts
        cat_mosdef = np.genfromtxt('/Users/michaeltopping/Documents/mosdef_zcat.final.dat', usecols=(2, 5))
        mosdef = {} 
        for row  in cat_mosdef:
            mosdef["{}".format(int(row[0]))] = row[1]

        addtl_zs = np.genfromtxt('morezs.init')
        for row in addtl_zs:
            mosdef["{}".format(int(row[0]))] = row[1]

        # loop through each object in the mask
        for obj in tqdm(objs[mask]):
            fitcat.write("#{}.{}\n".format(mask, obj))
        
            # assemble the filenames
            fnameb = loc+"b/split/{}.b.{}.msdfc_vs.fits".format(mask, obj)
            fnamer = loc+"r/split/{}.r.{}.msdfc_vs.fits".format(mask, obj)
            fnamebsig = loc+"b/split/{}.bsig.{}.msdfc_vs.fits".format(mask, obj)
            fnamersig = loc+"r/split/{}.rsig.{}.msdfc_vs.fits".format(mask, obj)

            # see if the object is in the catalog
            if obj in mosdef:        
                z = mosdef[obj]

                print("Measuring lines for {}.{} at z={}".format(mask, obj, z))

                # make sure the redshift is actually measured
                if z > 0:
        
                    # Make sure there is something to measure
                    try:
                        # measure all of the redshifts
                        measure_z(fnameb, fnamer, fnamebsig, fnamersig, z, obj, mask, fitcat)

                    except FileNotFoundError:
                        print("No spectra exist for this object")
                else:
                    print("{} {} z flagged as negative".format(mask, obj))
            else:
                print("no redshift found for {}".format(obj))
                


       
if __name__ == "__main__":

    # try to ignore DivideByZeroError sometimes thrown by scipy.optimize
#    with warnings.catch_warnings():
#        warnings.simplefilter("ignore")
#        run_fits()
    run_fits()

