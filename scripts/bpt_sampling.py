from bpt_assemble import *
import numpy as np
from math import sin, cos, atan
from scipy import interpolate
from stack_LRIS import *


def bpt_grid(ncols, nrows):
    bpt_xs, bpt_ys, bpt_IDs = plot_points()

    x1 = min(bpt_xs)
    x2 = max(bpt_xs)
    y1 = min(bpt_ys)
    y2 = max(bpt_ys)

    colwidth = (x2-x1)/ncols
    rowwidth = (y2-y1)/nrows


    for col in range(ncols+1):
        plt.plot([x1+col*colwidth, x1+col*colwidth], [y1, y2], 'k', linewidth=0.5, zorder=1)
    for row in range(nrows+1):
        plt.plot([x1, x2], [y1+row*rowwidth, y1+row*rowwidth], 'k', linewidth=0.5, zorder=1)


def locus_offset(xmin, xmax, dl, N):
    xs = np.linspace(xmin, xmax,N)
    curvexs = []
    curveys = []
    for x in xs:
        y = locus(x)
        slope = atan(perplocus(x))
        curvexs.append(x+dl*cos(slope))
        curveys.append(y+dl*sin(slope))

    return curvexs, curveys


def bpt_dist(xmin, xmax, levels, ndivs, plot_inset=True):
    fig = plt.figure()
    ax1 = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    bpt_xs, bpt_ys, bpt_IDs, catalog = plot_points(limittype='brightHA')
    divs = divisions(xmin, xmax, ndivs)

    # unravel the levels
    for ii, div in enumerate(divs[:-1]):
        l_below = levels[ii][0]
        l_above = levels[ii][-1]
        x = div
        y = locus(div)
        slope = atan(perplocus(div))
        plt.plot([x+l_below*cos(slope), x+l_above*cos(slope)], [y+l_below*sin(slope), y+l_above*sin(slope)], 'k', linewidth=0.5, zorder=1)

        # draw the normals again on the other side of the box
        x = divs[ii+1]
        y = locus(divs[ii+1])
        slope = atan(perplocus(x))
 
        plt.plot([x+l_below*cos(slope), x+l_above*cos(slope)], [y+l_below*sin(slope), y+l_above*sin(slope)], 'k', linewidth=0.5, zorder=1)


        for l in (levels[ii]):
            newxs, newys = locus_offset(div, divs[ii+1], l, 100)
            plt.plot(newxs, newys, 'k', linewidth=0.5, zorder=1)
            newxs, newys = locus_offset(div, divs[ii+1], l, 100)
            plt.plot(newxs, newys, 'k', linewidth=0.5, zorder=1)

        for jj in range(len(levels[ii])-1):
            inside, corners = get_cell_members(div, divs[ii+1]-div, 
                                levels[ii][jj], levels[ii][jj+1]-levels[ii][jj], bpt_xs, bpt_ys, shade=True)
            if plot_inset:
            # find the center of the cell
                cx = np.average([pos[0] for pos in corners])
                cy = np.average([pos[1] for pos in corners])
                curax = plt.gca()
    #            plt.figure()
                width=0.05
                height=0.05
                xloc = (cx + 2) / 2.
                yloc = (cy + .5) / 1.75
                xloc = (xloc - 0) / (1-.0)*.8+0.1-width/2
                yloc = (yloc - 0) / (1-.0)*.8+0.1-height/2
    #            xloc = (
                print(cx, cy, xloc, yloc)

                a = fig.add_axes([xloc, yloc, width, height])

                # this will make a stack of everything in the cell
                stack_spectra([bpt_IDs[i] for i in inside], 1.0, plotting='stack')

                # this will only make a stack for objects with certain 'catalog' parameters
                #stack_spectra([bpt_IDs[i] for i in inside if catalog[bpt_IDs[i]]['c4']], 1.0, plotting=True)

                plt.gca().tick_params(axis='x',labelbottom='off')
                plt.gca().tick_params(axis='y',labelleft='off')
                plt.gca().xaxis.set_ticks_position('none')
                plt.gca().yaxis.set_ticks_position('none')


                a.set_xlim([1530, 1570])

                plt.sca(curax)

    print(bpt_IDs)
    print(len(bpt_IDs))

def locus(x):
    return 0.61 / (x + 0.08) + 1.1

def dlocus(x):
    return - 0.61 / ( x + 0.08 )**2

def perplocus(x):
    return -1/dlocus(x)


def locus_length(xmin, xmax):
    xs = np.linspace(xmin, xmax, 100)
    # this is the locus funciton
    integrand = np.sqrt( 1+0.61**2 / (xs+0.08)**4 )

    return np.trapz(integrand, x=xs)

def get_cell_members(x, dx, r, dr, xs, ys, shade=False):
    N = 100
    # find the corners of the cell
    # start at the leftmost point
    c0, c1 = get_corners(x, r, dr)
    c2, c3 = get_corners(x+dx, r, dr)
    corners = [c0, c1, c2, c3]
        
    # define the arrays to hold the top boundary
    topxs = []
    topys = []
    # build up the top section of the cell
    # starting with the straight section
    # make an array of x values from the left corner to the end of the straight section
    tempxs = np.linspace(corners[0][0], corners[1][0], N)
    # rise over run
    slope = (corners[1][1] - corners[0][1])/(corners[1][0] - corners[0][0])
    # find the y values that follow the previously found slope
    tempys = [corners[0][1]+(ix-corners[0][0])*slope for ix in tempxs]
    topxs.append(tempxs)
    topys.append(tempys)
    # now the curved section
    tempxs = np.linspace(corners[1][0], corners[3][0], N)
    xtrash, tempys = locus_offset(x, x+dx, r+dr, N)
    topxs = np.append(topxs, tempxs)
    topys = np.append(topys, tempys)

    
    # now build up the bottom section
    botxs = []
    botys = []
    # start with the curved section
    tempxs = np.linspace(corners[0][0], corners[2][0], N)
    xtrash, tempys = locus_offset(x, x+dx, r, N)
    botxs.append(tempxs)
    botys.append(tempys)
    # now the straight seciton
    tempxs = np.linspace(corners[2][0], corners[3][0], N)
    slope = (corners[3][1] - corners[2][1]) / (corners[3][0] - corners[2][0])
    tempys = [corners[2][1] + (ix-corners[2][0])*slope for ix in tempxs]
    botxs = np.append(botxs, tempxs)
    botys = np.append(botys, tempys)
    

    # now create the functions for the top and bottom lines
    top = interpolate.interp1d(topxs, topys)
    bottom = interpolate.interp1d(botxs, botys)

    # loop through the values and return the indices that are within the box
    inside = []
    for ii, (ix, iy) in enumerate(zip(xs, ys)):
        # make sure it is in the correct x range
        if corners[0][0] < ix < corners[3][0]: 
            # see if it is between the top and bottom boundaries
            if bottom(ix) < iy < top(ix):
                inside.append(ii)
#                plt.scatter(ix, iy, s=80, facecolors='none', edgecolors='k')
    

    if shade:
        colors = plt.get_cmap('Blues')
        totxs = np.linspace(corners[0][0], corners[3][0], 200)
        plt.fill_between(totxs, top(totxs), bottom(totxs), color=colors(len(inside)/10.), zorder=1)
        
    return inside, corners


def get_corners(x, r, dr):
    # find the x,y point along the curve for 
    x_onlocus = x
    y_onlocus = locus(x)

    # get the slope perpendicluar to the curve
    slope = atan(perplocus(x_onlocus))

    # save the points along the normal at the requested distances
    x_offlocus_start = x_onlocus + r * cos(slope)
    y_offlocus_start = y_onlocus + r * sin(slope)
    x_offlocus_end = x_onlocus + (r+dr) * cos(slope)
    y_offlocus_end = y_onlocus + (r+dr) * sin(slope)

    return (x_offlocus_start, y_offlocus_start), (x_offlocus_end, y_offlocus_end)


def divisions(xmin, xmax, ndivs):
    totLength = locus_length(xmin, xmax)
    xs = np.linspace(xmin, xmax, 1000)
    lengths = []
    dl = totLength/ndivs
    
    for x in xs:
        lengths.append(locus_length(xmin, x))

    lengths = np.array(lengths)

    divs = []

    for ii in range(ndivs+1):
        
        # find the closest x value
        index = np.abs(lengths - (ii*dl)).argmin()

        divs.append(xs[index])

    return divs



    


if __name__ == "__main__":
    levels = [[-.2,0, .3], 
              [0, 0.15, 0.3], 
              [-.2,0, 0.15, 0.3, 0.45], 
              [-.2,0, 0.15, 0.3, 0.45], 
              [0, 0.15, 0.3], 
              [0, .15] ]
    bpt_dist(-1.6,-0.5,levels, 6, plot_inset=True)
#    plt.axes().set_aspect('equal')
#    plt.tight_layout()
#    bpt_grid(6, 5)
    plt.show()




