from read_spectrum import *
from scipy import interpolate
from plot_lines import *

# an example mask from Steidel+16
mask1 = [[1042, 1080],
         [1086, 1119],
         [1123, 1131],
         [1134, 1141],
         [1144, 1186],
         [1198, 1202],
         [1221, 1254],
         [1270, 1291],
         [1312, 1328],
         [1340, 1363],
         [1373, 1389],
         [1396, 1398],
         [1404, 1521],
         [1528, 1531],
         [1536, 1541],
         [1552, 1606],
         [1610, 1657],
         [1675, 1708],
         [1711, 1740],
         [1743, 1751],
         [1754, 1845],
         [1864, 1878],
         [1885, 1903],
         [1920, 2000]]
mask2 = [[1270, 1291],
         [1312, 1328],
         [1340, 1363],
         [1373, 1389],
         [1396, 1398],
         [1404, 1521],
         [1528, 1531],
         [1536, 1541],
         [1552, 1606],
         [1610, 1657],
         [1675, 1708],
         [1711, 1740],
         [1743, 1751],
         [1754, 1845],
         [1864, 1878],
         [1885, 1903],
         [1920, 2000]]



# go from our mask naming convention to field name
def field(mask):
    if mask[0:2] == 'co':
        return 'COSMOS'
    if mask[0:2] == 'gs':
        return 'GOODSS'
    if mask[0:2] == 'gn':
        return 'GOODSN'
    if mask[0:2] == 'ae':
        return 'AEGIS'


# for an object ID in our sample, return the name of the mask it is in
#  this only works if there are no object ID collisions between different fields
def get_mask(obj):
    locs, objs = load_data()
    for mask in objs:
        if obj in objs[mask]:
            return mask
    
    print("Cannot place obj:{}".format(obj))
    return None


# return a left and right index for an array  at a given wavelength and redshift
def ind_range(ws, w, z):
    lindex = np.abs(ws - w*(1+z)).argmin()

    return lindex


# input a wavelength and data array and a mask and this will return values within the mask
def mask_spectrum(ws, spectrum, mask):
    masked_ws = np.array([])
    masked_spectrum = np.array([])

    for sec in mask:
        # find the closest index
        lindex = ind_range(ws, sec[0], 0)
        rindex = ind_range(ws, sec[1], 0)

        # check to see if the closest index is inside our outside the array
        if ws[lindex] > sec[0]:
            lindex -= 1
        if ws[rindex] < sec[1]:
            rindex += 1

        masked_ws = np.append(masked_ws, ws[lindex+1:rindex])
        masked_spectrum = np.append(masked_spectrum, spectrum[lindex+1:rindex])

    return masked_ws, masked_spectrum


# shade the exluded regions of the mask
def draw_mask(mask):
    # get the current drawing axis
    ax = plt.gca()
    if ax is None:
        raise Exception("No drawing axis found")
    last_bound = 0
    for span in mask:
        ax.axvspan(last_bound, span[0], alpha=0.2, color='blue')
        last_bound = span[1]


# given a list of objects, retrieve all of the spectra and redshifts
def retrieve_data(objs):

    # find out which masks each object is in
    masks = []
    for obj in objs:
        masks.append(get_mask(obj))

    # create the arrays to hold the data
    w_arrs = []
    data_arrs = []

    # read in the data and put them in the arrays
    for obj, mask in zip(objs, masks):
        try:
            ws, spectrum, spectrum_sig = get_spectrum(obj, mask)
            w_arrs.append(ws)
            data_arrs.append(spectrum)
        except FileNotFoundError:
            print("Could not fine one or more files")

    # get the redshifts for all of the objects
    cat_mosdef = np.genfromtxt('/Users/michaeltopping/Documents/mosdef_zcat.final.dat', usecols=(2, 5))
    redshifts = {} 
    for row in cat_mosdef:
        redshifts["{}".format(int(row[0]))] = row[1]

    # print to see if all the redshifts exist
    for obj in objs:
        if obj not in redshifts:
            print("Object {} has no redshift and will not be included".format(obj))
    
    return w_arrs, data_arrs, redshifts


# get the minimum and maximum wavelength values from the sample
def get_w_bounds(objs, w_arrs, redshifts):
    minw = 9999
    maxw = 0
    for obj, ws in zip(objs, w_arrs):
        z = redshifts[obj]
        if (ws[0] / (1+z)) < minw:
            minw = ws[0]/(1+z)
        if (ws[-1] / (1+z)) > maxw:
            maxw = ws[-1]/(1+z)

    return minw, maxw


# input the list of objects and make a stack
def stack_spectra(objs, dw, plotting=False, plotting_style=None):

    # close if there are no objects in the stack
    if len(objs) == 0:
        return

    # read in all of the data
    w_arrs, data_arrs, redshifts = retrieve_data(objs)

    # get min and max wavelength values
    minw, maxw = get_w_bounds(objs, w_arrs, redshifts)

    # create the new wavelength array and data array for the stacked spectrum
    w_stack = np.arange(minw, maxw, dw)
    spectrum_stack = np.zeros(len(w_stack))

    # create the linear interpolations for the spectra
    spec_fs = []
    for ws, spectrum, obj in zip(w_arrs, data_arrs, objs):
        # find the normalization of the spectrum 
        # cut out a window surrouding the line of interest
        lindex = np.abs(ws - 1420*(1+redshifts[obj])).argmin()
        rindex = np.abs(ws - 1500*(1+redshifts[obj])).argmin()
        norm = np.median(spectrum[lindex:rindex])
        spec_fs.append(interpolate.interp1d(ws/(1+redshifts[obj]), spectrum/norm, fill_value='extrapolate'))

    # do a plot to make sure we're not crazy
    if plotting in ['individual', 'all']:
        for ii, spec in enumerate(spec_fs):
            plt.plot(w_stack, spec(w_stack), 'k', linewidth=0.2, alpha=0.01)

    # do the stacking!
    # loop through each spectrum
    for ii, w in enumerate(w_stack):
        sample = []

        for ws, spectrum, obj in zip(w_arrs, spec_fs, objs):
            z = redshifts[obj]
            if ws[0]/(1+z) < w < ws[-1]/(1+z):
                sample.append(spectrum(w))

        spectrum_stack[ii] = np.median(sample)

    print(np.shape(w_stack), np.shape(spectrum_stack))
#    plt.plot(w_stack, spectrum_stack, 'k', linewidth=2)
    if plotting in ['stack', 'all']:
        plt.ylim([-0.5, 2])
        if plotting_style == 'hist':
            plot_hist(plt.gca(), w_stack, spectrum_stack, 'black', linewidth=1)
        else:
            plt.plot(w_stack, spectrum_stack, 'k', linewidth=1)
        plt.plot([w_stack[0], w_stack[-1]], [0, 0], 'k:', linewidth=0.3)

    return w_stack, spectrum_stack


# plot an array as a histogram.  Useful for data like spectra
def plot_hist(ax, xs, ys, color, **kwargs):

    # draw on the given axis
    plt.sca(ax)

    # loop through each of the x values, and plot some vertical and horizontal lines to make up a histogram
    for ii in range(len(xs)-2):
        # this is half of a bin, and is the amount shifted over
        dx = (xs[ii+2] - xs[ii+1]) / 2.
        plt.plot([xs[ii+1]-dx, xs[ii+1]-dx], [ys[ii], ys[ii+1]], color=color, **kwargs)
        plt.plot([xs[ii+1]-dx, xs[ii+2]-dx], [ys[ii+1], ys[ii+1]], color=color, **kwargs)


def smooth(y, npts):
    box = np.ones(npts) / npts
    ysmooth = np.convolve(y, box, mode='same')
    return ysmooth


#  test function to make a stack of a bunch of objects
def test():
    fig = plt.figure(figsize=(14, 4))
#    objs = ['24020', '27216', '27120', '38559', '29834', '32354', '23409', '3668', '22669']
    objs = ['16545', '11530', '4156', '4711', '22931', '40768', '46938', '3666', '3626', '30053', '18543', '25817',
            '33942', '19439', '6283', '14957', '37226', '11968', '22939', '4078', '28659', '24020', '27216', '27120',
            '38559', '29834', '32354', '23409', '3668', '22669', '22669', '19985', '23344', '23869', '36257', '10494',
            '12476', '26557', '26557', '39897', '35262', '39713', '28846', '8515', '4497', '12345', '17038', '42363',
            '25142', '30074', '17437', '6311', '28421', '27627', '1740', '21955', '35705', '10066', '20924', '28710',
            '13364', '5571', '20924', '9801', '6963', '16547', '20062', '8232', '32638', '29650', '40679', '3324',
            '21279', '23040', '6569', '9148', '34308', '10471', '14739', '14739', '33768', '3974', '17204', '17204',
            '33801']
#    objs = ['1908']
#    objs = ['19985', '20062', '36705', '40768', '41547', '42363', '46938']
    ws, spectrum = stack_spectra(objs, 0.6, plotting='stack', plotting_style='hist')

    plt.xlim([800, 2000])
    plot_all(1)
    draw_mask(mask1)
    newws, newspectrum = mask_spectrum(ws, spectrum, mask1)
#    plt.plot(newws, newspectrum, '+', color='red')

    plt.show()


def continuum_test():
    fig = plt.figure(figsize=(14, 4))
#    objs = ['24020', '27216', '27120', '38559', '29834', '32354', '23409', '3668', '22669']
    objs = ['16545', '11530', '4156', '4711', '22931', '40768', '46938', '3666', '3626', '30053', '18543', '25817',
            '33942', '19439', '6283', '14957', '37226', '11968', '22939', '4078', '28659', '24020', '27216', '27120',
            '38559', '29834', '32354', '23409', '3668', '22669', '22669', '19985', '23344', '23869', '36257', '10494',
            '12476', '26557', '26557', '39897', '35262', '39713', '28846', '8515', '4497', '12345', '17038', '42363',
            '25142', '30074', '17437', '6311', '28421', '27627', '1740', '21955', '35705', '10066', '20924', '28710',
            '13364', '5571', '20924', '9801', '6963', '16547', '20062', '8232', '32638', '29650', '40679', '3324',
            '21279', '23040', '6569', '9148', '34308', '10471', '14739', '14739', '33768', '3974', '17204', '17204',
            '33801']
#    objs = ['1908']
    objs = ['19985', '20062', '36705', '40768', '41547', '42363', '46938']
    ws, spectrum = stack_spectra(objs, 0.6, plotting='stack', plotting_style='hist')
    newws, newspectrum = mask_spectrum(ws, spectrum, mask2)

    smooth_spec = smooth(newspectrum, 200)
    plt.plot(newws, smooth_spec, 'b', linewidth=2)
    plt.xlim([800, 2000])




if __name__ == "__main__":
#    test()
    continuum_test()
    plt.show()
    

