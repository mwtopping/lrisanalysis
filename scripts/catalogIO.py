from pathlib import Path



def read_zs(filename):

    zs = {}

    cat = Path(filename)
    with cat.open() as f:

        for line in f:
        
            if not line[0] == '#':
                line = line.strip().split()

                ID = line[0].split('.')[1]

                zs[ID] = {"zabs":line[1], "zabserr":line[2], 
                              "zem":line[3], "zemerr":line[4]}
            



    return zs

def read_info(zcat, c4s, snrcat):

    catalog = {}

    cat = Path(zcat)
    with cat.open() as f:

        for line in f:
        
            if not line[0] == '#':
                line = line.strip().split()

                ID = line[0].split('.')[1]

                catalog[ID] = {"zabs":line[1], "zabserr":line[2], 
                        "zem":line[3], "zemerr":line[4], "c4":0}
            

    cat = Path(c4s)
    with cat.open() as f:
        for line in f:
            ID = line.strip()
            if not ID in catalog:
                catalog[ID] = {'c4':1}
            else:
                catalog[ID]['c4'] = 1

    snrcat = Path(snrcat)
    with snrcat.open() as f:
        for line in f:
            line = line.strip().split()
            ID = line[0]
            if not line[1] == 'nan':
                snr = float(line[1])
            else:
                snr = 0
            
            catalog[ID]['snr'] = snr




    return catalog



if __name__ == "__main__":
    print(read_zs('z_final.cat'))
