from pathlib import Path

IDfile = Path('../data/match_ids_latest.txt')

v4ids = {}
targids = {}

with IDfile.open() as f:
    for line in f:
        if line[0] == '#':
           continue 
        line = line.strip().split()
        
        v4ids[line[3]] = line[5]
        targids[line[5]] = line[3]
        print(line[3], line[5])


def v4_to_targ(ID):
    return int(v4ids[str(ID)])

def targ_to_v4(ID):
    return int(targids[str(ID)])
