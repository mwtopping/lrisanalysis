import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import time
from scipy import interpolate

BPASS_ROOT = "/Volumes/RESEARCH/BPASSv2.2.1/"
micron = 1e-4 # convert from angstroms to microns

class PopModel:
    def __init__(self, binary, imf, maxmass, metallicity):
        self.binary = binary
        self.imf = imf
        self.maxmass = maxmass
        self.metallicity = metallicity

        if self.imf == 'chab':
            self.filename = BPASS_ROOT+\
                            "BPASSv2.2.1_{}-imf_chab{}/".format(self.binary,self.maxmass)+\
                            "spectra-{}-imf_chab{}.z{}.dat".format(self.binary, self.maxmass, self.metallicity)
        else:
            self.filename = BPASS_ROOT+\
                            "BPASSv2.2.1_{}-imf{}_{}/".format(self.binary,self.imf,self.maxmass)+\
                            "spectra-{}-imf{}-{}.z{}.dat".format(self.binary, self.imf, self.maxmass, self.metallicity)


        

    def load_data(self):
        data = np.genfromtxt(self.filename)
        self.wavelengths = data[:,0]
        self.spectra = data[:,1:]

    def make_consts(self):

        self.const_spectra = np.zeros(np.shape(self.spectra))
        for jj in range(np.shape(self.spectra)[1]):
            totSpectrum = np.zeros(np.shape(self.wavelengths))

            for ii in range(jj+1):
                if ii == 0:
                    totSpectrum += self.spectra[:,ii] * 10**6.05
                else:
                    totSpectrum += self.spectra[:,ii] * (10**(6.15+0.1*(ii)) - 10**(6.05+0.1*(ii)))
            self.const_spectra[:,jj] = totSpectrum

           


    def ind_to_age(self, ind):
        n = ind + 2
        return 10**(6+(n-2)/10.)

    def age_to_ind(self, age):
        return int(round(10*(np.log10(age)-6)+2)) - 2

    def plot_initial(self):
        try:
            initialSpectrum = self.spectra[:,0]
            plt.plot(self.wavelengths, initialSpectrum)
            plt.xlim([90, 3000])

        except NameError:
            print("You must load the data before plotting")


    def plot_const(self, num, norm, redden=0):
        if num < 100:
            index = num
        else:
            index = int(round(10*(np.log10(num)-6)+2)) - 2

        # find normalization factor
        lindex = np.abs(self.wavelengths - (1560)).argmin()
        rindex = np.abs(self.wavelengths - (1600)).argmin()
        
        model_norm = np.median(self.const_spectra[:,index][lindex:rindex]*self.redden(redden)[lindex:rindex])

        plt.plot(self.wavelengths, (norm/model_norm)*self.const_spectra[:,index]*self.redden(redden), 'k',alpha=0.2, linewidth=1)
        

    def get_piece_coeval(self, minw, maxw, age, redden=0):
        lindex = np.abs(self.wavelengths - minw).argmin()
        rindex = np.abs(self.wavelengths - maxw).argmin()

        age_ind = self.age_to_ind(age)

        return self.wavelengths[lindex:rindex],\
               self.spectra[:,age_ind][lindex:rindex]*self.redden(redden)[lindex:rindex]

    def get_piece_const(self, minw, maxw, age, redden=0):

        rate = 1

        totSpectrum = np.zeros(np.shape(self.wavelengths))

        for ii in range(self.age_to_ind(age)+1):
            if ii == 0:
                totSpectrum += rate * self.spectra[:, ii] * 10**6.05
            else:
                totSpectrum += rate * self.spectra[:, ii] * (10**(6.15+0.1*(ii)) - 10**(6.05+0.1*(ii)))



        lindex = np.abs(self.wavelengths - minw).argmin()
        rindex = np.abs(self.wavelengths - maxw).argmin()

        age_ind = self.age_to_ind(age)

        return self.wavelengths[lindex:rindex], totSpectrum[lindex:rindex]*self.redden(redden)[lindex:rindex]

    def redden(self, E):
        Rv = 4.05
        k = 2.659 * (-2.156 + 1.509/(self.wavelengths*micron) - 0.198/(self.wavelengths*micron)**2 +
                     0.011/(self.wavelengths*micron)**3) + Rv
        A = k * E

        # return the correction factor
        return 10**(-0.4*A)



def convolve(startws, endws, data):
    f = interpolate.interp1d(startws, data)
    return f(endws)


def plot_ryan(num, model):
    DIR = "/Volumes/RESEARCH/ryan/bpass_v2.2.1/"
    startime = time.time()
    data = np.genfromtxt(DIR+"cont-spectra-bin-imf_chab100.z002.dat")
    print("Loaded Ryan's data in {}s".format(time.time()-startime))
    wavelengths = data[:,0]
    spectra = data[:,1:]
    if num < 100:
        index = num
    else:
        index = int(round(10*(np.log10(num)-6)+2)) - 2

    plt.plot(wavelengths, spectra[:,index]*model.redden(0.191), linewidth=4)

def test():
    fig = plt.figure(figsize=(14,4))
    myModel = PopModel('bin', 'chab', '100', '002')
    myModel.load_data() 
    myModel.make_consts()
    plt.xlim([1000, 2000])
    ws = myModel.wavelengths
    spectrum = myModel.const_spectra[:,myModel.age_to_ind(1e8)]
    plot_ryan(myModel.age_to_ind(1e8), myModel)
    print(myModel.age_to_ind(1e8))
    plt.plot(ws, spectrum*myModel.redden(0.191), 'k')
#    newws = np.arange(900, 2000, 1.6)
#    plt.plot(newws, convolve(ws, newws, spectrum), 'k', linewidth=1)
    plt.show()


if __name__ == "__main__":
    test()




