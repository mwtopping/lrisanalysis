import numpy as np


BPASS_ROOT = BPASS_ROOT = "/Volumes/RESEARCH/BPASSv2.2.1/"

def make_bpass_const(binary, maxmass, metallicity):

    filename = BPASS_ROOT+\
    "BPASSv2.2.1_{}-imf_chab{}/".format(binary,maxmass)+\
    "spectra-{}-imf_chab{}.z{}.dat".format(binary, maxmass, metallicity)


    data = np.genfromtxt(filename)
    wavelengths = data[:,0]
    spectra = data[:,1:]

    const_spectra = np.zeros(np.shape(spectra))


    for jj in range(np.shape(spectra)[1]):
        totSpectrum = np.zeros(np.shape(wavelengths))

        for ii in range(jj+1):
            if ii == 0:
                totSpectrum += spectra[:,ii] * 10**6.05
            else:
                totSpectrum += spectra[:,ii] * (10**(6.15+0.1*(ii)) - 10**(6.05+0.1*(ii)))
                const_spectra[:,jj] = totSpectrum


    print(const_spectra)


def write_const_model(binary, maxmass, metallicity):

    filename = BPASS_ROOT+\
    "BPASSv2.2.1_{}-imf_chab{}/".format(binary,maxmass)+\
    "spectra-{}-imf_chab{}.z{}.dat".format(binary, maxmass, metallicity)



if __name__ == "__main__":
    make_bpass_const('bin', '100', '001')
