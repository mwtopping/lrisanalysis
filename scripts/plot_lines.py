import matplotlib.pyplot as plt
import numpy as np
from adjustText import adjust_text

# define all of the lines we want to plot
intabs=[('SiII',1260.4221),
('OI',1302.1685),
('SiII',1304.3702),
('CII',1334.5323),
('SiIV',1393.76018),
('SiIV',1402.77291),
('SiII',1526.70698),
('CIV',1548.204),
('CIV',1550.781),
('FeII',1608.45085),
('AlII',1670.7886),
('NiII',1741.553),
('NiII',1751.9),
('AlIII',1854.718),
('AlIII',1862.79)]

weakem=[('SiII',1264.73),
('SiII',1309.27),
('SiII',1533.43),
('OIII]',1660.8),
('OIII]',1666.15),
('CIII]',1908.73)]

photo=[('SiIII',1294.543),
('CIII',1296.33),
('SiIII',1296.73),
('CII',1323.93),
('NIII',1324.3),
('OIV',1343.35),
('SiIII',1417.2),
('CIII',1427.85),
('SV',1501.76),
('NIV',1718.55)]

Fe=[('FeII',2344.21),
('FeII*',2365.56),
('FeII',2374.46),
('FeII',2382.76),
('FeII*',2396.36),
('FeII',2586.65),
('FeII',2600.17),
('FeII*',2612.65),
('FeII*',2626.45),
('FeII*',2632.10)]

Lyman=[('Lyα',1215.67),
       ('Lyβ',1026),
       ('Lyγ',972)]


def plot_abs(norm):
    texts = []
    for line in intabs:
        plt.plot([line[1], line[1]], [-.2, 0], 'r')
        texts.append(plt.text(line[1], -0.2, line[0]))
    return texts

def plot_weak(norm):
    texts = []
    for line in weakem:
        plt.plot([line[1], line[1]], [0, 0.2], 'b')
        texts.append(plt.text(line[1], 0, line[0]))
    return texts

def plot_photo(norm):
    texts = []
    for line in photo:
        plt.plot([line[1], line[1]], [-0.1, 0.1], 'g')
        texts.append(plt.text(line[1], -0.1, line[0]))
    return texts    



def plot_fe(norm):
    texts = []
    for line in Fe:
        plt.plot([line[1], line[1]], [-0.3, -0.1], color='orange')
        texts.append(plt.text(line[1], -0.3, line[0]))
    return texts

def plot_Lyman(norm):
    texts = []
    for line in Lyman:
        plt.plot([line[1], line[1]], [-0.3, -0.1], color='red')
        texts.append(plt.text(line[1], -0.3, line[0]))
    return texts



def plot_all(norm):
    texts=plot_abs(norm)
    texts+=plot_weak(norm)
    texts+=plot_photo(norm)
    texts+=plot_fe(norm)
    texts+=plot_Lyman(norm)
#    adjust_text(texts, only_move={'text':'y'},arrowprops=dict(arrowstyle='->', color='red'))
        


