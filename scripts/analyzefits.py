import pathlib
import os
import astropy.io.fits
from collections import OrderedDict
import numpy as np
import matplotlib.pyplot as plt


LAMBDA_MID = 5000
FIT_WINDOW = 50

linelist = OrderedDict({"SiIIa":1260.4221, "OI+SiII":1303.26935, "CII":1334.5323, "SiIIb":1526.70698,"FeII":1608.45085, "AlII":1670.7886, "LyA":1215.67})
LyA = 1215.67


# this is a procedure that readin in the fitting parameters from a file
# INPUT: filename - the filename of the fitting parameter file
def read_fits(filename):

    fits = OrderedDict({}) 

    fitsfile = pathlib.Path(filename)

    mask = "aaaaa"
    specline = "bbbbb"
    obj = "99999"
    
    # open the file and loop through the lines
    with fitsfile.open() as f:
        for line in f:
            # this will be a line that contains an object identifier
            if line.startswith('#'):
                line = line[1:].strip().split('.')
                mask = line[0]
                obj = line[1]
                # if this is the first object that has been seen in this mask, create the data structure to hold it
                if not mask in fits:
                    fits[mask] = {}
                
                # create the data structure to hold the object fitting data
                fits[mask][obj] = {}

            # check if the first character is a letter, this will be the start of a new line measurement
            elif line[0].isalpha() and not (line.split()[0] == 'nan'):

                specline = ''.join(i for i in line if (i.isalpha() or i == '+'))
                fits[mask][obj][specline] = []

            # this will be a line that is fitting parameters
            elif (line[0].isdigit() or line[0] == '-'):
                fits[mask][obj][specline].append(line.strip())


    return fits
            
            

# absorption line used for fitting
def abs(x, *args):
    a, b, d, s, mu = args
    return a + b*x + d / (s * np.sqrt(2*np.pi)) * np.exp(-0.5*( (x-mu) / s**2)**2)

# emission line used for fitting
def em(x, *args):
    a, b, d, s, mu = args
    return a + b*x +  d / (s * np.sqrt(2*np.pi)) * np.exp(-0.5*( (x-mu) / s**2)**2)
#    return a + b*x + c*(x-e)**2+ d / (s * np.sqrt(2*np.pi)) * np.exp(-0.5*( (x-mu) / s**2)**2)


# read in the fits data and return the spectrum and wavelength information
# INPUT:  filename - fits file of spectrum
# OUTPUT: data  - data array containing the spectrum data
#         CRVAL - wavelength of the first pixel
#         CDELT - wavelength spacing of the data
#         naps  - number of apertures in the file
def read_spectrum(filename):
    # read the data into a buffer
    buf = astropy.io.fits.getdata(filename)
    ap=1
    
    # check if the data has more than one aperture
    if len(np.array(buf).shape) > 1:
        data = buf
        naps = np.array(buf).shape[0]
    else:
        data = buf
        naps = 1
    # get the header data
    header = astropy.io.fits.getheader(filename)

    # read in the wavelength solution if there is only one aperture
    if 'CRVAL1' in header.keys():
        CRVAL = header['CRVAL1'] #zero point
        CDELT = header['CDELT1']

    # there are more than 1 apertures
    else:
        
        sol = header["WAT2_00{}".format(ap)].strip()
    
        sol = sol.split('{} {} 0'.format(ap, ap))
        sol = sol[-1].strip().split(" ")

        CRVAL=float(sol[0])
        CDELT=float(sol[1])

    return data, CRVAL, CDELT, naps



# this will create all of the axes objects and set up the figure for plotting
def generate_plots():
    # generate the initial figure
    fig = plt.figure(figsize=(14,7))

    # create the axes objects for each of the spectral line fits
    ax1 = plt.subplot2grid((2,6), (0,0))
    ax2 = plt.subplot2grid((2,6), (0,1))
    ax3 = plt.subplot2grid((2,6), (0,2))
    ax4 = plt.subplot2grid((2,6), (0,3))
    ax5 = plt.subplot2grid((2,6), (0,4))
    ax6 = plt.subplot2grid((2,6), (0,5))
    ax7 = plt.subplot2grid((2,6), (1,0)) # lya

    # this is the axis for the full spectrum 
    ax8 = plt.subplot2grid((2,6), (1,1), colspan=4)

    # this is the axis to display the redshift measurements
    ax9 = plt.subplot2grid((2,6), (1,5))
    plt.setp(ax9.get_xticklabels(), visible=False)
    ax9.yaxis.tick_right()
    ax9.yaxis.set_label_position("right")
    ax9.set_ylabel('$z$')

    axs = [ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9]

    # remove the tick labels for everything but the z plot
    for ax in axs[:-1]:
        ax.yaxis.get_offset_text().set_visible(False)
        plt.setp(ax.get_yticklabels(), visible=False)

    # only for the absorption line panls
    for ax in axs[:-2]:
        set_ax_color(ax, 'green')
    return fig, axs


# read in the spectra from the red and blue side of lris
# INPUT: bfilename - filename of the blue side spectrum
#        rfilename - filename of the red side spectrum
# OUTPUT: ws_b      - wavelength array for the blue side spectrum
#         spectra_b - data for the blue side spectrum
#         ws_r      - wavelength array for the red side spectrum
#         spectra_r - data array for the red side spectrum
def get_spectra(bfilename, rfilename):
    # make sure the blue side spectrum exists
    try:
        data, CRVAL, CDELT, naps = read_spectrum(bfilename)
        ws_b = CRVAL + CDELT*np.arange(len(data))
        spectra_b = data
    except FileNotFoundError:
        print("Missing {}".format(bfilename))
        ws_b = []
        spectra_b = []

    # make sure the red side spectrum exists
    try:
        data, CRVAL, CDELT, naps = read_spectrum(rfilename)
        ws_r = CRVAL + CDELT*np.arange(len(data))
        spectra_r = data
    except FileNotFoundError:        
        print("Missing {}".format(rfilename))
        ws_r = []
        spectra_r = []


    return ws_b, spectra_b, ws_r, spectra_r


# plot all of the spectral lines and their errors
# INPUT: ws           - this is the array of wavelengths
#        spectrum     - the spectrum data array
#        spectrum_sig - this is the sigma spectrum data
#        linelist     - this is the dictionary of lines and their wavelengths
#        axs          - the array of axes objects
#        z            - the redshift estimate of the object
def plot_lines(ws, spectrum, spectrum_sig, linelist, axs, z):

    # this is the size of the window surrounding each line
    window = 50
    
    # loop through each of the lines
    for ii, line in enumerate(linelist):
        w = linelist[line]
        # find the indices on the left and right side of the line
        lindex = np.abs(ws - (w*(1+z) - window)).argmin()
        rindex = np.abs(ws - (w*(1+z) + window)).argmin()

        # plot the grey area for the error
        axs[ii].fill_between(ws[lindex:rindex], spectrum[lindex:rindex]-spectrum_sig[lindex:rindex], spectrum[lindex:rindex]+spectrum_sig[lindex:rindex], alpha=0.2)
        # plot the spectrum as a histogram
        plot_hist(axs[ii],ws[lindex:rindex], spectrum[lindex:rindex], '#1f77b4')
        # label the plot with the line
        axs[ii].text(0.1, 0.95,line,verticalalignment='center',
             transform = axs[ii].transAxes)


# plot the full spectrum in the panel
# INPUT: ws           - this is the array of wavelengths
#        spectrum     - the spectrum data array
#        spectrum_sig - this is the sigma spectrum data
#        axs          - the array of axes objects
def plot_spectrum(ws, spectrum, spectrum_sig, axs): 

    axs[-2].fill_between(ws, spectrum-spectrum_sig, spectrum+spectrum_sig, color='black', alpha=0.2)
    axs[-2].plot(ws, spectrum, 'k', linewidth=0.5)
    axs[-2].set_xlabel(r'$\rm \AA$')
    axs[-2].set_ylabel(r'$F$')

# plot a spectrum as a histogram
def plot_hist(ax, xs, ys, color, **kwargs):

    plt.sca(ax)

    # loop through each of the x values, and plot some vertical and horizontal lines to make up a histogram
    for ii in range(len(xs)-2):
        plt.plot([xs[ii+1]-.5, xs[ii+1]-.5], [ys[ii], ys[ii+1]], color=color, **kwargs)
        plt.plot([xs[ii+1]-.5, xs[ii+2]-.5], [ys[ii+1], ys[ii+1]], color=color, **kwargs)


# plot the lyman alpha line
def plot_lya(ws, spectrum, spectrum_sig, axs, z):
    window = 50
    lindex = np.abs(ws - (LyA*(1+z) - window)).argmin()
    rindex = np.abs(ws - (LyA*(1+z) + window)).argmin()

    axs[6].fill_between(ws[lindex:rindex], spectrum[lindex:rindex]-spectrum_sig[lindex:rindex], spectrum[lindex:rindex]+spectrum_sig[lindex:rindex], alpha=0.2)
   
    plot_hist(axs[6],ws[lindex:rindex], spectrum[lindex:rindex], '#1f77b4')
    axs[6].text(0.1, 0.95,"LyA",verticalalignment='center',
         transform = axs[6].transAxes)



# given a red and blue side spectrum this will put them together into a single spectrum
def splice_spectrum(ws_b, spectra_b, ws_r, spectra_r):

    # check to make sure at least one side of the spectra is available
    if len(spectra_b) == 0 and len(spectra_r) == 0:
        print("No spectra available")
        raise FileNotFoundError

    # this is if there is only a red side spectrum
    elif len(spectra_b) == 0:

        rindex = np.array([x for x in ws_r - LAMBDA_MID if x < 0]).argmax()+1 # index to the right of the middle
        ws = ws_r[rindex:] 
        spectrum = spectra_r[rindex:]

    # this is if there is only a blue side spectrum
    elif len(spectra_r) == 0:

        bindex = np.array([x for x in ws_b - LAMBDA_MID if x < 0]).argmax() # index to the left of the middle
        ws = ws_b[:bindex] 
        spectrum = spectra_b[:bindex]


    # there are both red and blue spectra
    else:
        # make sure the dichroic cutoff is within the spectra, otherwise, dont cut the spectrum
        try:
            rindex = np.array([x for x in ws_r - LAMBDA_MID if x < 0]).argmax()+1 # index to the right of the middle
        except ValueError:
            rindex = 0
        try:
            bindex = np.array([x for x in ws_b - LAMBDA_MID if x < 0]).argmax() # index to the left of the middle
        except ValueError:
            bindex = 0
        ws = np.append(ws_b[:bindex], ws_r[rindex:])
        spectrum = np.append(spectra_b[:bindex], spectra_r[rindex:])

    return ws, spectrum


# plot all of the redshifts calculated for each line
# INPUT: line_meas - the dictionary containing the measured redshift for each line
#        ax        - the axis to plot it in
#        z         - the initial redshift measurement
def plot_zs(line_meas, axs, z):
    labels = []

    # plot the initial redshift measurement
    axs[-1].plot([0,6], [z,z], 'k')
     
    # loop through each of the lines and plot them on their axes
    for ii, line in enumerate(line_meas):
        if line == "LyA":
            z_meas = line_meas[line][0] / LyA - 1
            z_high = (line_meas[line][0]+line_meas[line][1]) / LyA - 1 - z_meas
            z_low = (line_meas[line][0]-line_meas[line][1]) / LyA - 1 - z_meas
            axs[-1].errorbar(ii, z_meas, yerr=[[z_low, z_high]], fmt='r')
            axs[-1].plot(ii, z_meas, 'ro')
            labels.append("LyA")

        else:
            z_meas = line_meas[line][0] / linelist[line] - 1
            z_high = (line_meas[line][0]+line_meas[line][1]) / linelist[line] - 1 - z_meas
            z_low = (line_meas[line][0]-line_meas[line][1]) / linelist[line] - 1 - z_meas
            axs[-1].errorbar(ii, z_meas, yerr=[[z_low, z_high]], fmt='b')
            axs[-1].plot(ii, z_meas, 'bo')
            labels.append(line)

    # more plot formatting options 
    plt.sca(axs[-1])
    plt.xticks(range(len(labels)), labels, rotation=-90)
    locs=list(axs[-1].get_yticks())
    labels = ["{:.3f}".format(x) for x in locs]
    labels.append(r'$z_{\rm MOSDEF}$')
    locs.append(z)
    plt.yticks(locs, labels)
    #plt.setp(ax.get_yticklabels(), visible=True)
    plt.setp(axs[-1].get_xticklabels(), visible=True)


    # loop through the other axes and plot the measured redshift
    for ii, line in enumerate(line_meas):
        plt.sca(axs[ii])
        plt.axvline(x=line_meas[line][0])


# this sets the borter of ax to have a color
def set_ax_color(ax, color):
    
    ax.spines['top'].set_color(color)
    ax.spines['left'].set_color(color)
    ax.spines['bottom'].set_color(color)
    ax.spines['right'].set_color(color)
    ax.spines['top'].set_linewidth(3)
    ax.spines['left'].set_linewidth(3)
    ax.spines['bottom'].set_linewidth(3)
    ax.spines['right'].set_linewidth(3)


# this is a procedure that is called when the plot is clicked
def onclick(event, axs, usable):
    # this determines which axis is clicked
    clicked=(np.where([ax == event.inaxes for ax in axs]))[0]
    # only activate when one of the spectral line plots are clicked
    if clicked < 7:
        # toggle the 'usable' flag for that plot
        usable[clicked] = (0 if usable[clicked]==1 else 1)

    
    # change all of the colors to their appropriate values
    update_colors(axs, usable)
    # redraw the plot
    plt.gcf().canvas.draw()
    

# for each axis object, set the color based on the 'usable' value for that axis
def update_colors(axs, usable):
    for ii, u in enumerate(usable):
        if u:
            set_ax_color(axs[ii], 'green')
        else:
            set_ax_color(axs[ii], 'red')
    


# plot all of the fits for each spectral line
# INPUT: ws      - array of wavelengths
#        z       - redshift estimate
#        axs     -  array of axis objects
#        fitdata - dictionary of all the fit parameters
#        obj     - object identifier
#        mask    - name of the mask that the object is on
def plot_fits(ws, z, axs, fitdata, obj, mask):

    # loop through each of the spectral lines
    for ii, line in enumerate(linelist):
        w = linelist[line]

        # pick out the part of the spectra we want
        lindex = np.abs(ws - (w*(1+z) - FIT_WINDOW)).argmin()
        rindex = np.abs(ws - (w*(1+z) + FIT_WINDOW)).argmin()

        subws = ws[lindex:rindex] 
        
        # loop through each set of fitting parameters and plot them on the axis
        for fit in fitdata[mask][obj][line]:
            params = [float(x) for x in fit.strip().split(' ')]
            axs[ii].plot(subws, 1e-29*abs(subws, *params), color="#cb5658", alpha = 0.05)


# this gets the average redshift and error estimates from all of the fitting data
def analyze_fits(fitdata, obj, mask):
    # dictionary to hold the redshift data
    line_meas = OrderedDict({})

    # loop through each of the spectral line
    for line in linelist:
        means = []
        # loop through all of the fitting data and find the average redshift
        for fit in fitdata[mask][obj][line]:
        
            params = [float(x) for x in fit.strip().split(' ')]
            means.append(params[4])

        line_meas[line] = (np.median(means), np.std(means))

    # once for LyA
    means = []
    for fit in fitdata[mask][obj]["LyA"]:
        params = [float(x) for x in fit.strip().split(' ')]
        means.append(params[4])

    center = np.median(means)
    # calculate the error via the median aboslute deviation for normally distribute ddata
    err = 1.4826 * MAD(means)

    line_meas["LyA"] = (center, err)
       

    return line_meas



# calculate the median absolute deviation (MAD) of an array
def MAD(xs):
    med = np.median(xs)
    return np.median(np.abs(xs - med))


# load in all of the object identifiers and data directory locations
def load_data():


    objs = {}
    locs = {}
#    objs['gs_l1'] = ['31791','31344','32837','36705','40768','37988','31854','33248','41547','45188','35178','40679','42556','39198','41218','41886','35705',    '46335','34114','38119','42363','38559','45180','46938','35779','42809','45531','39713','40218','38116']
    
    locs['gs_l1'] = "/Users/michaeltopping/lrisData/gs_l1-combine/"
    objs['gs_l1'] = ['35178']

#    objs['co_l1'] = ['11968','11530','11443','11153','10056','10143','9801','9094','8540','8338','8081','7912','7273','6963','6826','6417','5912','5686','546    2','4945','4154','3694','3112','2672','2786','2207','1908','1740','1382','964','541','307','241']
    objs['co_l1'] = ['3112']
    locs['co_l1'] = "/Users/michaeltopping/lrisData/lris_jan17/co_l1/"

#    objs['co_l2'] = ['16545','16547','17038','17233','18067','19985','20062','19439','19712','21743','20171','21780','22939','21955','23134','23663','23183',    '23210','23841','24427','24020','24414','24053','24738','26073','25322','27216','27120','26332','28258','27906']
    objs['co_l2'] = ['20062']
    locs['co_l2'] = "/Users/michaeltopping/lrisData/lris_jan17/co_l2/"

#    objs['gn_l1']=['30053','32526','29743','26621','30461','31955','30709','28846','29834','21772','23344','27035','23869','18128','25142','21279','17958','2    1845','22023','20924','16713','19654','18161','15186','12980','12157','10596','12345','10645']
    objs['gn_l1'] = ['10596']
    locs['gn_l1'] = "/Users/michaeltopping/lrisData/lris_apr17/gn_l1/"

#    objs['ae_l1']=['30074','36257','36451','33808','32354','24481','38356','23409','34661','23040','28710','21675','17437','16496','16730','15082','25522','1    6121','14957','26153','12918','17085','10471','10494','3668','6569','11729','14880','4711','6311','3112']
    objs['ae_l1'] = ['12918', '6569', '28710']
    locs['ae_l1']= "/Users/michaeltopping/lrisData/lris_apr17/ae_l1/"

#    objs['co_l5']=['4078','4497','3666','6018','4446','3974','6379','3626','7430','7735','3185','6283','851','6179','5107','7883','3324','4029','10066','1008    5','4441','3757','8697','4156','5162','6743']
    objs['co_l5'] = ['4441', '4029', '4930']
    locs['co_l5'] = "/Users/michaeltopping/lrisData/lris_jan2018/co_l5/"

#    objs['co_l6']=['13101','13299','15710','12476','11716','13364','12577','12611','14423','10235','11343','10280','9044','10835','9148','10093','8679','8515    ','6413','9251','5814','8232','6817','5901','5571','4962']
    objs['co_l6'] = ['6413']
    locs['co_l6'] = "/Users/michaeltopping/lrisData/lris_jan2018/co_l6/"

#    objs['gn_l3'] = ['21617','11892','14739','17940','17204','22235','17667','19067','19350','16351','17714','17530','22299','25498','24825','24388','22669',    '24846','25505','22487','23674','25688','28237','28599','26557','29095','24328']
#    objs['gn_l3'] = ['11892','14739','17940','17204','22235','17667','19067','19350','16351','17714','17530','22299','25498','24825','24388','22669','24846',    '25505','22487','23674','25688','28237','28599','26557','29095','24328']
    objs['gn_l3'] = ['17940']
    locs['gn_l3'] = "/Users/michaeltopping/lrisData/gn_l3-combine/"

#    objs['ae_l3']=['33768','28659','34308','31226','33801','22931','20924','39897','30278','40851','28421','27825','18543','33973','34813','32638','34848','3    7226','32761','24341','27627','35262','25817','29650','33942']
    objs['ae_l3'] = ['20924']
    locs['ae_l3'] = "/Users/michaeltopping/lrisData/ae_l3-combine/"
    return locs, objs


# controller script to start the gui in order to inspect the fits
# INPUT: fnameb    - fiilename of the blue side spectrum
#        fnamer    - filename of the red side spectrum
#        fnamebsig - filename of the blue side error spectrum
#        fnamersig - filename of the red side error spectrum
#        z         - redshift estimate of the object
#        obj       - object identifier
#        mask      - the name of the mask the object lies on
#        fitdata   - the dictionary that contains all fitting parameters
def inspect_fits(fnameb, fnamer, fnamebsig, fnamersig, z, obj, mask, fitdata):

    # initialize all plots to be 'good'
    usable = np.ones(7)
    
    # load in and accumulate all of the spectra
    ws_b, spectra_b, ws_r, spectra_r = get_spectra(fnameb, fnamer)
    ws_bsig, spectra_bsig, ws_rsig, spectra_rsig = get_spectra(fnamebsig, fnamersig)

    ws, spectrum = splice_spectrum(ws_b, spectra_b, ws_r, spectra_r)
    ws_sig, spectrum_sig = splice_spectrum(ws_bsig, spectra_bsig, ws_rsig, spectra_rsig)

    if len(spectrum) < len(spectrum_sig):
        spectrum_sig = spectrum_sig[0:len(spectrum)]
    elif len(spectrum) > len(spectrum_sig):
        spectrum = spectrum[0:len(spectrum_sig)]
        ws = ws[0:len(spectrum_sig)]


    # generate all of the axes objects
    fig, axs = generate_plots()


    # plot the spectral line data and full spectrum
    plot_lines(ws, spectrum, spectrum_sig, linelist, axs, z)
    plot_spectrum(ws, spectrum, spectrum_sig, axs)
    plot_lya(ws, spectrum, spectrum_sig, axs, z)

    # plot all of the gaussians defined by the fitting parameters
    plot_fits(ws, z, axs, fitdata, obj, mask)
    
    # combine the fitting parameters into a single value and error
    line_meas = analyze_fits(fitdata, obj, mask)
    
    # plot all of the calculated zs
    plot_zs(line_meas, axs, z)

    # plot formatting
    plt.suptitle("{}.{}".format(mask.replace('_','\_'),obj))

    # allow the figure to be clicked
    cid = plt.gcf().canvas.mpl_connect('button_press_event', lambda event: onclick(event, axs, usable))

    # show and save a copy of the final figure
    plt.show()
    fig.savefig("fitoutput/{}.{}.pdf".format(mask, obj))

    # print the redshifts for each line to the console
    for line in line_meas:
        print('{}         '.format(line), end='')
    print()
    for ii, line in enumerate(line_meas):
        
        if usable[ii]:
            z = line_meas[line][0] / linelist[line] - 1 
            dz = (line_meas[line][0] + line_meas[line][1]) / linelist[line] - 1 - z

            print('{:.4f}+/-{:.4f}   '.format(z, dz), end='')
        else:
            print('-2.000+/-0.000   ', end='')
    print()
    
    # ask for any notes on this object
    ans = input('Object notes?:')
    note = ans.split('note')[-1]
        

    return line_meas, usable, note


# read in the redshift data from the catalogs
# INPUT: cats - a filename, or list of filenames of the redshift data
def read_catalogs(cat):
    
    # create the dictionary to store data
    objs = []


    # loop through each catalog, even if there is only 1
    
    # open up and loop through each line of the file
    catfile = pathlib.Path(cat)
    with catfile.open() as f:
        for line in f:
            # exclude comments
            if line[0] == '#':
                pass
            else:
                # convert the file to a list of numbers
                line = list(filter(None, line.strip().split(' ')))
                ID = line[0]
    
                objs.append(ID)
        
    return objs

    


# loop through all of the data and start the program
def run_inspection():
    masks = ['gs_l1', 'co_l1', 'co_l2', 'gn_l1', 'ae_l1', 'co_l5', 'co_l6', 'gn_l3', 'ae_l3']
    masks = ['gs_l1', 'co_l1', 'gn_l1', 'ae_l1', 'co_l5', 'co_l6', 'gn_l3', 'ae_l3']
    masks = ['gn_l3']
#    masks = ['gn_l1', 'co_l5', 'gn_l3']
    # get all of the fitting parameters and load object filenames
    fitdata = read_fits("fits_more.cat")
    locs, objs = load_data()
    
    print(fitdata.keys())

    maskstart = 0

    
    # loop through all of the different masks
    for mask in masks[maskstart:]:
        
        existingdata = []

        # create a file to save the redshift data in
        loc = locs[mask]
        catfile = '../data/{}.addz.cat'.format(mask)
        print(catfile, os.path.isfile(catfile))
        if os.path.isfile(catfile):
            ans = input('Would you like to delete {} and start over?'.format(catfile))
            if ans.lower().startswith('y'):
                cat = open(catfile, 'w')
                cat.write('#ID      ')
                for line in linelist:
                    cat.write('{} d{}   '.format(line, line))
                cat.write('\n')


            else:
                existingdata = read_catalogs("../data/{}.z.cat".format(mask))
                cat = open(catfile, 'a')
        else:
            cat = open(catfile, 'w')
            cat.write('#ID      ')
            for line in linelist:
                cat.write('{} d{}   '.format(line, line))
            cat.write('\n')


        
        # this is reads in the initial redshift measurements
        cat_mosdef = np.genfromtxt('/Users/michaeltopping/Documents/mosdef_zcat.final.dat', usecols=(2, 5))
        mosdef = {} 
        for row  in cat_mosdef:
            mosdef["{}".format(int(row[0]))] = row[1]

        addtl_zs = np.genfromtxt('morezs.init')
        for row in addtl_zs:
            mosdef["{}".format(int(row[0]))] = row[1]

        # loo pthrough each object in the mask
        for obj in objs[mask]:

            if not obj in existingdata:
                cat.write('{}   '.format(obj))
                
                fnameb = loc+"b/split/{}.b.{}.msdfc_vs.fits".format(mask, obj)
                fnamer = loc+"r/split/{}.r.{}.msdfc_vs.fits".format(mask, obj)
                fnamebsig = loc+"b/split/{}.bsig.{}.msdfc_vs.fits".format(mask, obj)
                fnamersig = loc+"r/split/{}.rsig.{}.msdfc_vs.fits".format(mask, obj)
                # make sure that the object has a redshift estimate
                if obj in mosdef:        

                    z = mosdef[obj]

                    print("Measuring lines for {}.{} at z={}".format(mask, obj, z))

                    if (z > 0) and (len(fitdata[mask][obj]) > 0):
                        try:
                            # get the redshift measurements form the gui
                            line_meas, usable, note = inspect_fits(fnameb, fnamer, fnamebsig, fnamersig, z, obj, mask, fitdata)

                            for ii, line in enumerate(line_meas):
                                # if the line was tagged as usable
                                if usable[ii]:
                                    z = line_meas[line][0] / linelist[line] - 1
                                    dz = (line_meas[line][0] + line_meas[line][1]) / linelist[line] - 1 - z
                                    cat.write('{:.4f} {:.4f}   '.format(z, dz))
                                else:
                                    cat.write('-2.000 -2.000   ')
                            cat.write(note)
                        except FileNotFoundError:
                            print("No spectra exist for this object")
                            for line in linelist:
                                cat.write('-2.000 -2.000   ')
                            cat.write('no spectra were found')
                            
                    
                
                    else:
                        for line in linelist:
                            cat.write('-2.000 -2.000   ')
     
                        cat.write('no fits or z < 0')
                else:
                    for line in linelist:
                        cat.write('-2.000 -2.000   ')
      
                    cat.write('no available redshift'.format(obj))
                    

                cat.write('\n')
        cat.close() 



if __name__ == "__main__":

    # run the code
    run_inspection()
