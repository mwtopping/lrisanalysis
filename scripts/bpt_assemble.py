from keys import v4_to_targ, targ_to_v4
import matplotlib
from collections import OrderedDict
import numpy as np
import astropy.io.fits
from pathlib import Path
import matplotlib.pyplot as plt
from matplotlib import rc
from catalogIO import read_info
from read_sdss import *

# change plotting parameters
rc('font', **{'serif': ['Computer Modern'], 'size': 12})
rc('text', usetex=True)

def read_mosdef(filename):

    mosdeffile = astropy.io.fits.open(filename)
    mosdefdata = mosdeffile[1]


    return mosdefdata

def read_objects(filename):

    objects = []

    objfile = Path(filename)
    with objfile.open() as f:
        for line in f:
            line = line.strip()
            objects.append(line)
            
    return objects

def plot_sdss(filename):
    Zs = get_column('../data/gal_info_dr7_v5_2.fit', 'Z')


    OIIIs = get_column('../data/gal_line_dr7_v5_2.fit', 'OIII_5007_FLUX')
    HBs = get_column('../data/gal_line_dr7_v5_2.fit', 'H_BETA_FLUX')
    NIIs = get_column('../data/gal_line_dr7_v5_2.fit', 'NII_6584_FLUX')
    HAs = get_column('../data/gal_line_dr7_v5_2.fit', 'H_ALPHA_FLUX')

    OIIIerr = get_column('../data/gal_line_dr7_v5_2.fit', 'OIII_5007_FLUX_ERR')
    HBerr = get_column('../data/gal_line_dr7_v5_2.fit', 'H_BETA_FLUX_ERR')
    NIIerr = get_column('../data/gal_line_dr7_v5_2.fit', 'NII_6584_FLUX_ERR')
    HAerr = get_column('../data/gal_line_dr7_v5_2.fit', 'H_ALPHA_FLUX_ERR')

    

    #Zs = get_column('../data/gal_line_dr7_v5_2.fit', 'HA6565_Z')

    xs = []
    ys = []

    for ii in range(len(HAs)):
        try:
            if ((OIIIs[ii]/OIIIerr[ii]/1.566) > 5) and ((HBs[ii]/HBerr[ii]/1.882) > 5) and ((NIIs[ii]/NIIerr[ii]/2.039) > 5) and ((HAs[ii]/HAerr[ii]/2.473) > 5):
                if 0.04 < Zs[ii] < 0.10:
                    xs.append(np.log10(NIIs[ii] / HAs[ii]))
                    ys.append(np.log10(OIIIs[ii] / HBs[ii]))
        except: 
            print("Divide by zero")





    xbins = np.arange(-2, 0.5, 0.02)
    ybins = np.arange(-1.25, 1.5, 0.02)

    hist = np.histogram2d(xs, ys, bins=[xbins, ybins])
#
    xbincenters = (xbins[:-1] + xbins[1:]) / 2.
    ybincenters = (ybins[:-1] + ybins[1:]) / 2.

#    cnt = plt.contourf(xbincenters, ybincenters, np.log(hist[0].T), 200, cmap='binary')
#    for c in cnt.collections:
#        c.set_edgecolor("face")
    img = np.log10(1+hist[0])
    img[img==-np.inf] = 0
    plt.imshow(img.T, extent=(-2.0, 0.5, -1.25, 1.5), origin='lower', cmap='binary', aspect='auto')

def get_col(mosdefdata, ID):

    try:
        v4ID = v4_to_targ(ID)
        matches = np.where(mosdefdata.data.field('ID')==v4ID)[0]
    except KeyError:
        return None

    if len(matches) > 1:
        print("More than one match {} in the line catalog for ID {}:{}".format(matches, ID, mosdefdata.data.field('ID')))
        for m in matches:
            print(mosdefdata.data[m].field('ID'))

    else:
        return matches[0]

def get_val(mosdefdata, ID, field):

    col = get_col(mosdefdata, ID)

    if col == None:
        return -999.0
    else:
        return mosdefdata.data[col].field(field)

def get_style(method, catalog, obj):

    style = {'markersize':3, 'color':'black', 'marker':'o'}

    if method == 'redshift':
        if obj in catalog:
            style['markersize'] = 5
            style['marker'] = 's'
            if (float(catalog[obj]['zem']) > 0 and float(catalog[obj]['zabs']) > 0):
                style['color'] = '#7e3c99'
            elif (float(catalog[obj]['zem']) > 0):
                style['color'] = '#e41a1c'
            elif (float(catalog[obj]['zabs']) > 0):
                style['color'] = '#0c4fc8'
    if method == 'c4':
        if obj in catalog:
            style['markersize'] = 5
            style['marker'] = 's'
            style['label'] = 'no CIV'
            if catalog[obj]['c4']:
                style['color'] = 'green'
                style['label'] = 'CIV'

    if method == 'snr':
        colors = plt.get_cmap('plasma')
        if obj in catalog:
            style['markersize'] = 5
            style['marker'] = 's'
            style['color'] = colors(catalog[obj]['snr']/5.)
#            if catalog[obj]['snr'] > 4:
#                print("snr4")
#            
#            if catalog[obj]['snr'] > 5:
#                print("snr5")
    if method == 'none':
        if obj in catalog:
            style['markersize'] = 5
            style['marker'] = 'o'
            style['color'] = '#80b1d3'


    return style



def get_lists():
    objects = read_objects('../data/objects.txt')
    mosdefdata = read_mosdef('../data/linemeas_latest.fits')

    OIIIs = []
    HBs = []
    NIIs = []
    HAs = []

    OIII_errs = []
    HB_errs = []
    NII_errs = []
    HA_errs = []

    Zs = []
 
    
    for obj in objects:
        OIIIs.append(get_val(mosdefdata, obj, 'OIII5008_PREFERREDFLUX'))
        HBs.append(get_val(mosdefdata, obj, 'HB4863_PREFERREDFLUX'))
        NIIs.append(get_val(mosdefdata, obj, 'NII6585_PREFERREDFLUX'))
        HAs.append(get_val(mosdefdata, obj, 'HA6565_PREFERREDFLUX'))

        OIII_errs.append(get_val(mosdefdata, obj, 'OIII5008_PREFERREDFLUX_ERR'))
        HB_errs.append(get_val(mosdefdata, obj, 'HB4863_PREFERREDFLUX_ERR'))
        NII_errs.append(get_val(mosdefdata, obj, 'NII6585_PREFERREDFLUX_ERR'))
        HA_errs.append(get_val(mosdefdata, obj, 'HA6565_PREFERREDFLUX_ERR'))

#    print(get_val(mosdefdata, '28237', 'OIII5008_PREFERREDFLUX'))
#    print(get_val(mosdefdata, '28237', 'HB4863_PREFERREDFLUX'))
#    print(get_val(mosdefdata, '28237', 'NII6585_PREFERREDFLUX'))
#    print(get_val(mosdefdata, '28237', 'HA6565_PREFERREDFLUX'))
#    print(get_val(mosdefdata, '28237', 'ID'))

    




            
    return objects, np.array(OIIIs), np.array(HBs), np.array(NIIs), np.array(HAs), \
            np.array(OIII_errs), np.array(HB_errs), np.array(NII_errs), np.array(HA_errs)



def plot_points(limittype='brightHA'):
#    fig = plt.figure()
    objects, OIIIs, HBs, NIIs, HAs, OIII_errs, HB_errs, NII_errs, HA_errs = get_lists()
    catalog = read_info('../data/z_final.cat', '../data/c4.txt', 'snr.txt')
    flags = read_flags("../data/compile_agn_flags.txt")
    xs = np.linspace(-2, -0.09, 1000)
    ys = 0.61 / (xs + 0.08) + 1.1
    plt.plot(xs, ys, '--', linewidth=2, color='#ca0020')

    bpt_xs = []
    bpt_ys = []
    bpt_IDs = []
 
    arrowLength = 0.10
    nplotted = 0
    for ii in range(len(HAs)):

        style = {'markersize':3, 'color':'black', 'marker':'o'}
        


        obj = objects[ii]
        try:
            print(obj, v4_to_targ(obj) )
        except:
            print("Not in id file")
        if not (OIIIs[ii] < -1 or HBs[ii] < -1 or NIIs[ii] < -1 or HAs[ii] < -1):

            # find the error in the x direction
            xerr = np.sqrt( ( NII_errs[ii] / (NIIs[ii]*np.log(10)))**2 + 
                            ( HA_errs[ii]  / (HAs[ii]*np.log(10)))**2 )
            # y direction
            yerr = np.sqrt( ( OIII_errs[ii] / (OIIIs[ii]*np.log(10)))**2 + 
                            ( HB_errs[ii]  / (HBs[ii]*np.log(10)))**2 )
 
            
#            if obj in zs:
#                style['markersize'] = 5
#                style['marker'] = 's'
#                if (float(zs[obj]['zem']) > 0 and float(zs[obj]['zabs']) > 0):
#                    style['color'] = '#7e3c99'
#                elif (float(zs[obj]['zem']) > 0):
#                    style['color'] = '#e41a1c'
#                elif (float(zs[obj]['zabs']) > 0):
#                    style['color'] = '#0c4fc8'
            style = get_style('snr', catalog, obj)
            if obj in flags:
                if 5 > flags[obj] > 0:
                    style['marker'] = '*'
                    style['markersize'] = 8
                    style['color'] = '#2ca25f'
            else:
                print("{} not in flag file".format(obj))
            





#--------------------------------  All limits
            if limittype == 'all':
                if NIIs[ii] / NII_errs[ii] < 3 and HAs[ii] / HA_errs[ii] < 3:
                    print("NII and HA")
                    continue
                if OIIIs[ii] / OIII_errs[ii] < 3 and HBs[ii] / HB_errs[ii] < 3:
                    print("OIII and HB")
                    continue
                if NIIs[ii] / NII_errs[ii] < 3:
                    if OIIIs[ii] / OIII_errs[ii] < 3:
                       #plt.errorbar(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0,uplims=True, xuplims=True)
                        plt.arrow(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), -arrowLength,0,color='#222222',fc='#dddddd',linewidth=0.5, alpha=1.0, head_width=0.035, head_length=0.02, )
                        plt.arrow(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), 0,-arrowLength*1.5,color='#222222',fc='#dddddd',linewidth=0.5, alpha=1.0, head_width=0.03, head_length=0.03, )

                        plt.plot(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)
                        print(OIIIs[ii], OIII_errs[ii], OIIIs[ii]/OIII_errs[ii])
                        nplotted += 1
                        continue

                    elif HBs[ii] / HB_errs[ii] < 3:
    #                    plt.errorbar(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0,lolims=True, xuplims=True)
                        plt.arrow(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), -arrowLength, 0,color='#222222',fc='#dddddd',linewidth=0.5, alpha=1.0,head_width=0.035, head_length=0.02, )
                        plt.arrow(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), 0, arrowLength*1.5,color='#222222',fc='#dddddd',linewidth=0.5, alpha=1.0,head_width=0.03, head_length=0.03, )
                        plt.plot(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), markeredgewidth=0.5,markeredgecolor='black',**style)
                        nplotted += 1
                        continue

                    else:
                        plt.errorbar(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10(OIIIs[ii]/HBs[ii]), yerr=yerr,color='#222222',linewidth=0.5, alpha=1.0)
                        plt.arrow(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10(OIIIs[ii]/HBs[ii]),-arrowLength, 0,ec='#222222',linewidth=0.5, alpha=1.0, head_width=0.035, head_length=0.02,fc='#dddddd')
                        plt.plot(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10(OIIIs[ii]/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)
                        nplotted += 1
                        continue

                elif HAs[ii] / HA_errs[ii] < 3:
                    if OIIIs[ii] / OIII_errs[ii] < 3:
                        plt.errorbar(np.log10((NIIs[ii])/(HAs[ii]+3*HA_errs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0,uplims=True, xlolims=True)
                        plt.plot(np.log10((NIIs[ii])/(HAs[ii]+3*HA_errs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)
                        nplotted += 1
                        continue

                    elif HBs[ii] / HB_errs[ii] < 3:
                        plt.errorbar(np.log10((NIIs[ii])/(HAs[ii]+3*HA_errs[ii])), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0,lolims=True, xlolims=True)
                        plt.plot(np.log10((NIIs[ii])/(HAs[ii]+3*HA_errs[ii])), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), markeredgewidth=0.5,markeredgecolor='black',**style)
                        nplotted += 1
                        continue

                    else:
                        plt.errorbar(np.log10((NIIs[ii])/(HAs[ii]+3*HA_errs[ii])), np.log10(OIIIs[ii]/HBs[ii]), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0, xlolims=True)
                        plt.plot(np.log10((NIIs[ii])/(HAs[ii]*3*HA_errs[ii])), np.log10(OIIIs[ii]/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)
                        nplotted += 1
                        continue

                if OIIIs[ii] / OIII_errs[ii] < 3:
    #                if NIIs[ii] / NII_errs[ii] < 3:
    #                    plt.errorbar(np.log10((NIIs[ii]+3*NII_errs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=0.2,uplims=True, xuplims=True)
    #                    plt.plot(np.log10((NIIs[ii]+3*NII_errs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)
    #                    continue
    #
    #                if HAs[ii] / HA_errs[ii] < 3:
    #                    plt.errorbar(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/(HBs[ii])), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=0.2,lolims=True, xlolims=True)
    #                    plt.plot(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+OIII_errs[ii])/(HBs[ii])), markeredgewidth=0.5,markeredgecolor='black',**style)
    #                    continue

                    #plt.errorbar(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0, uplims=True)
                    plt.errorbar(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0)
                    plt.arrow(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), 0,-arrowLength*1.5,color='#222222',linewidth=0.5,fc='#dddddd', alpha=1.0, head_width=0.03, head_length=0.03, )
                    plt.plot(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)
                    nplotted += 1
                    continue
#
#
#            if HBs[ii] / HB_errs[ii] < 3:
#
#                #plt.errorbar(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0, lolims=True)
#                plt.errorbar(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0)
#                plt.arrow(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), 0, arrowLength*1.5,color='#222222',linewidth=0.5, alpha=1.0, head_width=0.03, head_length=0.03, fc='#dddddd')
#                plt.plot(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), markeredgewidth=0.5,markeredgecolor='black',**style)
#                nplotted += 1
#                continue
#
#--------------------------------




#--------------------------------   NII limits with bright HA
            if limittype == 'brightHA':
                if NIIs[ii] / NII_errs[ii] < 3 and HAs[ii] / HA_errs[ii] < 3:
                    continue
                if OIIIs[ii] / OIII_errs[ii] < 3 and HBs[ii] / HB_errs[ii] < 3:
                    continue
                if NIIs[ii] / NII_errs[ii] < 3:
                    if OIIIs[ii] / OIII_errs[ii] < 3:
                        #plt.errorbar(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0,uplims=True, xuplims=True)
#                        plt.arrow(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), -xerr,0,color='#222222',fc='#dddddd',linewidth=0.5, alpha=1.0, head_width=0.035, head_length=0.02, )
#                        plt.arrow(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), 0,-yerr,color='#222222',fc='#dddddd',linewidth=0.5, alpha=1.0, head_width=0.03, head_length=0.03, )

#                        plt.plot(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)
                        continue

                    elif HBs[ii] / HB_errs[ii] < 3:
#                        plt.errorbar(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0,lolims=True, xuplims=True)
#                        plt.arrow(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), -xerr, 0,color='#222222',fc='#dddddd',linewidth=0.5, alpha=1.0,head_width=0.035, head_length=0.02, )
#                        plt.arrow(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), 0, yerr,color='#222222',fc='#dddddd',linewidth=0.5, alpha=1.0,head_width=0.03, head_length=0.03, )
#                        plt.plot(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), markeredgewidth=0.5,markeredgecolor='black',**style)
                        continue

                    if HAs[ii] > 5e-17 and HAs[ii] / HA_errs[ii] > 3:
#                        plt.errorbar(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10(OIIIs[ii]/HBs[ii]), yerr=yerr,color='#222222',linewidth=0.5, alpha=1.0)
#                        plt.arrow(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10(OIIIs[ii]/HBs[ii]),-arrowLength, 0,ec='#222222',linewidth=0.5, alpha=1.0, head_width=0.035, head_length=0.02,fc='#dddddd')
#                        plt.plot(np.log10((NIIs[ii]+3*NII_errs[ii])/HAs[ii]), np.log10(OIIIs[ii]/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)
#                        print(ii)
#                        print(HAs[ii], HA_errs[ii], HAs[ii]/HA_errs[ii])
#                        print(NIIs[ii], NII_errs[ii], NIIs[ii]/NII_errs[ii])
#                        print(xerr, yerr)
#                        nplotted += 1

                        continue
                    continue

                elif HAs[ii] / HA_errs[ii] < 3:
                    if OIIIs[ii] / OIII_errs[ii] < 3:
                        #plt.errorbar(np.log10((NIIs[ii])/(HAs[ii]+3*HA_errs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0,uplims=True, xlolims=True)
                        #plt.plot(np.log10((NIIs[ii])/(HAs[ii]+3*HA_errs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)
                        continue

                    elif HBs[ii] / HB_errs[ii] < 3:
                        #plt.errorbar(np.log10((NIIs[ii])/(HAs[ii]+3*HA_errs[ii])), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0,lolims=True, xlolims=True)
                        #plt.plot(np.log10((NIIs[ii])/(HAs[ii]+3*HA_errs[ii])), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), markeredgewidth=0.5,markeredgecolor='black',**style)
                        continue

                    else:
                        #plt.errorbar(np.log10((NIIs[ii])/(HAs[ii]+3*HA_errs[ii])), np.log10(OIIIs[ii]/HBs[ii]), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0, xlolims=True)
                        #plt.plot(np.log10((NIIs[ii])/(HAs[ii]*3*HA_errs[ii])), np.log10(OIIIs[ii]/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)
                        continue

                if OIIIs[ii] / OIII_errs[ii] < 3:
                    if NIIs[ii] / NII_errs[ii] < 3:
                        #plt.errorbar(np.log10((NIIs[ii]+3*NII_errs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=0.2,uplims=True, xuplims=True)
                        #plt.plot(np.log10((NIIs[ii]+3*NII_errs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)
                        continue

                    if HAs[ii] / HA_errs[ii] < 3:
                        #plt.errorbar(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/(HBs[ii])), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=0.2,lolims=True, xlolims=True)
                        #plt.plot(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+OIII_errs[ii])/(HBs[ii])), markeredgewidth=0.5,markeredgecolor='black',**style)
                        continue

                    #plt.errorbar(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0, uplims=True)
                    #plt.errorbar(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0)
                    #plt.arrow(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), 0,-yerr,color='#222222',linewidth=0.5,fc='#dddddd', alpha=1.0, head_width=0.03, head_length=0.03, )
                    #plt.plot(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii]+3*OIII_errs[ii])/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)

                    continue


                if HBs[ii] / HB_errs[ii] < 3:

                    #plt.errorbar(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), yerr=yerr, xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0, lolims=True)
                    #plt.errorbar(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), xerr=xerr,color='#222222',linewidth=0.5, alpha=1.0)
                    #plt.arrow(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), 0, yerr,color='#222222',linewidth=0.5, alpha=1.0, head_width=0.03, head_length=0.03, fc='#dddddd')
                    #plt.plot(np.log10((NIIs[ii])/(HAs[ii])), np.log10((OIIIs[ii])/(HBs[ii]+3*HB_errs[ii])), markeredgewidth=0.5,markeredgecolor='black',**style)
                    continue
#--------------------------------




            plt.errorbar(np.log10(NIIs[ii]/HAs[ii]), np.log10(OIIIs[ii]/HBs[ii]),xerr=xerr, yerr=yerr, linewidth=0.5,color='#222222', alpha=1.0)

            plt.plot(np.log10(NIIs[ii]/HAs[ii]), np.log10(OIIIs[ii]/HBs[ii]), markeredgewidth=0.5,markeredgecolor='black',**style)
            bpt_xs.append(np.log10(NIIs[ii]/HAs[ii]))
            bpt_ys.append(np.log10(OIIIs[ii]/HBs[ii]))
            bpt_IDs.append(obj)
            nplotted += 1

#                plt.text(np.log10(NIIs[ii]/HAs[ii]), np.log10(OIIIs[ii]/HBs[ii])+0.05, obj, horizontalalignment='center', fontsize=5)
        else:
            print("No detection")


    # set up the legend
#    plt.plot(-100, -100, label='not reduced', marker='o', color='black', markersize=3, linewidth=0)
#    plt.plot(-100, -100, label='no redshift', marker='s', color='black', markersize=5, linewidth=0)
#    plt.plot(-100, -100, label=r'has $z_{\rm em}$', marker='s', color='#e41a1c', markersize=5, linewidth=0)
#    plt.plot(-100, -100, label=r'has $z_{\rm abs}$', marker='s', color='#0c4fc8', markersize=5, linewidth=0)
#    plt.plot(-100, -100, label=r'has $z_{\rm em}$+$z_{\rm abs}$', marker='s', color='#7e3c99', markersize=5, linewidth=0)

    print("Plotted {} points out of a possible {}".format(nplotted, len(HAs)))
        
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.xlim([-2,0.0])
    plt.ylim([-0.5,1.25])
    #plot_sdss('../data/gal_line_dr7_v5_2.fit')

    plt.xlabel(r'$\rm log([NII]6584/H\alpha)$')
    plt.ylabel(r'$\rm log([OIII]5007/H\beta)$')

#    plt.plot(-100, -100, color='#377eb8', markersize=5, markeredgewidth=0.5, markeredgecolor='black', marker='o', label='flag 0', linestyle="None")
#    plt.plot(-100, -100, color='#377eb8', markersize=5, markeredgewidth=0.5, markeredgecolor='black', marker='^', label='flag 1', linestyle="None")
#    plt.plot(-100, -100, color='#377eb8', markersize=5, markeredgewidth=0.5, markeredgecolor='black', marker='s', label='flag 2', linestyle="None")
#    plt.plot(-100, -100, color='#377eb8', markersize=5, markeredgewidth=0.5, markeredgecolor='black', marker='p', label='flag 3', linestyle="None")
#    plt.plot(-100, -100, color='#377eb8', markersize=5, markeredgewidth=0.5, markeredgecolor='black', marker='H', label='flag 4', linestyle="None")
#    plt.plot(-100, -100, color='#377eb8', markersize=7, markeredgewidth=0.5, markeredgecolor='black', marker='*', label='flag $>4$', linestyle="None")
#    
#    plt.legend(by_label.values(), by_label.keys(), loc='lower left')
#    plt.legend(loc='lower left')
    norm = matplotlib.colors.Normalize(vmin=0, vmax=5)
#    cbax = fig.add_axes([0.9, 0.11, 0.03, 0.78])
#    cb1 = matplotlib.colorbar.ColorbarBase(cbax, cmap=plt.get_cmap('plasma'), norm=norm)
#    plt.show()
#    plt.savefig("../figures/LRIS_bpt.pdf")
    return bpt_xs, bpt_ys, bpt_IDs, catalog



def read_flags(filename):
    flags = {}
    flagfile = Path(filename)
    with flagfile.open() as f:
        for line in f:

            line = line.strip().split()
            ID = line[1]
            flag = int(line[2])
            flags[ID] = flag
            print(ID, flag)
    
    return flags


def main():
    plot_points()







if __name__ == "__main__":
    main()



