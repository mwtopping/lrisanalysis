from catalogIO import read_zs
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

# change plotting parameters
rc('font', **{'serif': ['Computer Modern'], 'size': 14})
rc('text', usetex=True)

c = 3e5


def find_offset():

    zs = read_zs('z_final.cat')
    IDs = []

    cat_mosdef = np.genfromtxt('/Users/michaeltopping/Documents/mosdef_zcat.final.dat', usecols=(2, 5))
    mosdef = {} 
    for row  in cat_mosdef:
        mosdef["{}".format(int(row[0]))] = row[1]

    zabs = []
    zem = []
    zsys = []

    for obj in zs:
        if obj in mosdef:
            zabs.append(float(zs[obj]['zabs']))
            zem.append(float(zs[obj]['zem']))
            
            zsys.append(float(mosdef[obj]))
            IDs.append(obj)


    zabs = np.array(zabs)
    zem = np.array(zem)
    zsys = np.array(zsys)


    absoffset = ( (zabs-zsys) / (1+zsys)) * c
    emoffset = ( (zem-zsys) / (1+zsys))  * c

    for ii in range(len(IDs)):
        
        if np.abs(emoffset[ii]) < 1000:
            if emoffset[ii] < 0:
                print(IDs[ii], emoffset[ii])

    absoffset[absoffset >= 1e308] = -2000
    emoffset[emoffset >= 1e308] = -2000

    absoffset[absoffset <= -1e308] = -2000
    emoffset[emoffset <= -1e308] = -2000
#
#
    bins = np.linspace(-0.01, 0.01, 20)
    bins = np.linspace(-400, 600, 20)
#    plt.hist(zabs-zsys, bins=bins)
#    plt.hist(zem-zsys, bins=bins)
    plt.hist(absoffset, bins=bins, label=r"$ v_{\rm LIS,abs}$ ", color='#377eb8')
    plt.hist(emoffset, bins=bins, label=r"$v_{\rm Ly\alpha,em}$", color='#e41a1c')


    plt.xlabel("Velocity Offset (km/s)")
    plt.ylabel("$N$")
    plt.legend(loc='upper right', fontsize=14)
    plt.savefig("../figures/z_offset.pdf")
   # plt.show()
    



    

if __name__ == "__main__":
    find_offset()

    
